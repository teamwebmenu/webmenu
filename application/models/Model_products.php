<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_products extends CI_Model {

	public function get_products()
	{
		$this->db->join('categories', 'categories.cat_id = products.cat_id', 'left');
		$query = $this->db->get('products');
		return $query->result();
	}

	public function get_categories()
	{
		$query = $this->db->get('categories');
		return $query->result();
	}

	public function save_category()
	{
		$object = array('cat_name' => $this->input->post('cat_name'));
		$query = $this->db->insert('categories', $object);
		return $query;
	}

	public function save_product()
	{
		$object = array(
			    'description' => $this->input->post('description'),
			    'prod_desc' => $this->input->post('prod_desc'),
			    'cat_id' => $this->input->post('cat_id'),
			    'unit_price' => $this->input->post('unit_price')
		);

		$query = $this->db->insert('products', $object);
		return $query;
	}

	public function product_details($id)
	{
		$this->db->where('product_id', $id);
		$query = $this->db->get('products');
		return $query->result();
	}

	public function updateProduct($id)
	{
		$data = array(
			'description' => $this->input->post('description') ,
			'prod_desc' => $this->input->post('prod_desc'),
			'cat_id' => $this->input->post('cat_id'),
			'unit_price' => $this->input->post('unit_price') 
		);

		$this->db->where('product_id', $id);
		$query = $this->db->update('products', $data);

		return $query;

	}

	public function deleteProduct($id)
	{
		$this->db->where('product_id', $id);
		$query = $this->db->delete('products');

		return $query;
	}

	public function category_details($id)
	{
		$this->db->where('cat_id', $id);
		$query = $this->db->get('categories');
		return $query->result();
	}

	public function updateCategory($id)
	{
		$data = array(
			'cat_name' => $this->input->post('cat_name')
		);

		$this->db->where('cat_id', $id);
		$query = $this->db->update('categories', $data);

		return $query;
	}

	public function category_used($id)
	{
		$this->db->where('cat_id', $id);
		$query = $this->db->get('products');

		return $query->num_rows();
	}

	public function deleteCategory($id)
	{
		$this->db->where('cat_id', $id);
		$query = $this->db->delete('categories');

		return $query;
	}

	public function updateProductImg($id,$img)
	{
		$data = array(
			'upload_image' => $img . '.jpg'
		);

		$this->db->where('product_id', $id);
		$query = $this->db->update('products', $data);

		return $query;
	}
}

/* End of file Model_products.php */
/* Location: ./application/models/Model_products.php */