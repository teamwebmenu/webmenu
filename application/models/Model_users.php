<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_users extends CI_Model {

	public function get_users()
	{
		$query = $this->db->get('users');
		return $query->result();
	}

	public function saveUser()
	{
		$data = array(
			'username' => strtolower(mb_substr($this->input->post('first_name'),0,1) . $this->input->post('last_name')),
			'fname' => $this->input->post('first_name'),
			'lname' => $this->input->post('last_name'),
			'password' => md5($this->input->post('password')),
			'position' => $this->input->post('position'),
			'access_level' => $this->input->post('access_level'),
			'is_active' => 1
		);

		$query = $this->db->insert('users', $data);

		return $query;
	}

	public function user_details($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('users');

		return $query->row_array();
	}

	public function updateUser($id)
	{
		$data = array(
			'fname' => $this->input->post('first_name'),
			'lname' => $this->input->post('last_name'),
			'position' => $this->input->post('position'),
			'access_level' => $this->input->post('access_level'),
		);
		$this->db->where('id', $id);
		$query = $this->db->update('users', $data);

		return $query;
	}

	public function isActive($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('users');

		return $query->row_array();
	}

	public function enableAccount($id)
	{
		$object = array('is_active' => 1 );
		$this->db->where('id', $id);
		$query = $this->db->update('users', $object);

		return $query;
	}

	public function disableAccount($id)
	{
		$object = array('is_active' => 0 );
		$this->db->where('id', $id);
		$query = $this->db->update('users', $object);

		return $query;
	}

	public function add_customer()
	{
		$data = array(
			'username' => $this->input->post('mobile_number') ,
			'fname' => $this->input->post('first_name') , 
			'lname' => $this->input->post('last_name') ,
			'province' => $this->input->post('province'),
			'municipality' => $this->input->post('municipality'),
			'barangay' => $this->input->post('barangay'),
			'address' => $this->input->post('address'),
			'mobile_number' => $this->input->post('mobile_number') ,
			'email' => $this->input->post('email') ,
			'password' => md5($this->input->post('password')),
			'position' => 'Customer',
			'is_active' => 1,
			'access_level' => 2
		);

		$query = $this->db->insert('users', $data);

		return $query;

	}

	public function customer_exists($username)
	{
		$this->db->where('username', $username);
		$query = $this->db->get('users');
		return $query->num_rows();
	}

	public function updateUserImg($id,$img)
	{
		$data = array(
			'image' => $img . '.jpg'
		);

		$this->db->where('id', $id);
		$query = $this->db->update('users', $data);

		return $query;
	}

}

/* End of file Model_users.php */
/* Location: ./application/models/Model_users.php */