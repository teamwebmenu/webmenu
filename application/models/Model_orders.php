<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_orders extends CI_Model {


	public function listOrders()
	{
		$this->db->select('*');
		$this->db->select('COUNT(order_details.order_id) AS item_count');
		$this->db->select('SUM(order_details.unit_price * order_details.quantity) AS total_amount');
		$this->db->select('CONCAT(users.fname," ",users.lname) AS customer_name');
		$this->db->join('order_details', 'order_details.order_id = orders.order_id', 'left');
		$this->db->join('users', 'users.id = orders.user_id', 'left');
		$this->db->group_by('orders.order_id');
		$query = $this->db->get('orders');
		return $query->result();
	}

	public function my_order($user_id)
	{
		// $this->db->select('*');
		// $this->db->select('COUNT(order_details.order_id) AS item_count');
		// $this->db->select('SUM(order_details.unit_price) AS total_amount');
		// $this->db->select('CONCAT(users.fname," ",users.lname) AS customer_name');
		// $this->db->join('order_details', 'order_details.order_id = orders.order_id', 'left');
		// $this->db->join('users', 'users.id = orders.user_id', 'left');
		// $this->db->where('orders.user_id', $user_id);
		// $this->db->group_by('orders.order_id');

		// //$this->db->where('orders.user_id', $user_id);

		// $query = $this->db->get('orders');
		$query = $this->db->query("

			SELECT 
			 orders.*,
			 orders.order_id AS cart_id,
			 COUNT(order_details.order_id) AS item_count,
			 SUM(order_details.unit_price * order_details.quantity) AS total_amount,
			 CONCAT(users.fname,' ',users.lname) AS customer_name
			 
			FROM orders

			LEFT JOIN order_details
			ON order_details.order_id = orders.order_id
			LEFT JOIN users
			ON users.id = orders.user_id
			WHERE orders.user_id = " . $user_id .
			" GROUP BY orders.order_id");

		return $query->result();
	}

	public function my_order_details($order_id)
	{

		$query = $this->db->query("
			SELECT * FROM order_details 
			LEFT JOIN orders
			ON orders.order_id = order_details.order_id
			LEFT JOIN products
			on products.product_id = order_details.product_id
			WHERE order_details.order_id = " . $order_id);

		return $query->result();
	}

	public function removeItem($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete('order_details');

		return $query;
	}

	public function delete_order($id){

		$this->db->where('order_id', $id);
		//$query = $this->db->delete('orders');
		$object = array('order_status' => 3 );
		$query = $this->db->update('orders', $object);

		return $query;
	}

	public function submitOrder($cart_id)
	{
		$this->db->where('order_id', $cart_id);
		$object = array('order_status' => 1 );
		$query = $this->db->update('orders', $object);
		return $query;
	}

	public function acknowledgeOrder($cart_id)
	{
		$this->db->where('order_id', $cart_id);
		$object = array('order_status' => 2 );
		$query = $this->db->update('orders', $object);
		return $query;
	}

	public function readyOrder($cart_id)
	{
		$this->db->where('order_id', $cart_id);
		$object = array('order_status' => 4 );
		$query = $this->db->update('orders', $object);
		return $query;
	}

	public function payOrder($cart_id)
	{
		date_default_timezone_set("Asia/Manila");
		$this->db->where('order_id', $cart_id);
		$object = array(
			'order_status' => 5,
			'date_paid' => date('Y-m-d H:i:s')
		);
		$query = $this->db->update('orders', $object);

		return $query;
	}

	public function invoiceHeader($cart_id){
		$query = $this->db->query("
			SELECT orders.*,
			SUM(order_details.unit_price * order_details.quantity) AS total_cost,
			COUNT(order_details.order_id) AS item_count,
			orders.date_paid AS date_paid,
			CONCAT(users.fname,' ', users.lname) AS customer_name,
			users.address AS customer_street,
			users.barangay AS customer_barangay,
			users.municipality AS customer_municipality,
			users.mobile_number AS contact_number

			FROM orders

			LEFT JOIN order_details
			ON order_details.order_id = orders.order_id

			LEFT JOIN users
			ON users.id = orders.user_id

			WHERE orders.order_id = " . $cart_id .
			" GROUP BY orders.order_id");

			return $query->result();
	}

	//ADMIN DASHBOARD STATS
	public function get_total_sales()
	{
		$query = $this->db->query("
			select
			sum(order_details.quantity * order_details.unit_price) as total_amount
			from orders
			left join order_details
			on orders.order_id = order_details.order_id
			where orders.order_status = 5
			group by orders.order_id
			");

		return $query->result();
	}

	public function count_orders()
	{
		$query = $this->db->query("select COUNT(*) as count_orders from orders");
		return $query->result();
	}

	public function pending_orders()
	{
		$query = $this->db->query("select COUNT(*) as count_orders from orders where order_status = 3");
		return $query->result();
	}

	public function cancelled_orders()
	{
		$query = $this->db->query("select COUNT(*) as count_orders from orders where order_status = 0");
		return $query->result();
	}

	public function paid_orders()
	{
		$query = $this->db->query("select COUNT(*) as count_orders from orders where order_status = 5");
		return $query->result();
	}

	//order details on submit
	public function order_details_on_submit($cart_id)
	{
		$query = $this->db->query("
			SELECT
			 products.description,
			 order_details.quantity,
			 order_details.unit_price,
			 (order_details.quantity * order_details.unit_price) as total_price
			FROM order_details
			INNER JOIN products
			ON order_details.product_id = products.product_id
			WHERE order_id =" . $cart_id);

		return $query->result();
	}

	//add notes to order
	public function addOrderNotes($cart_id,$order_notes)
	{
		$object = array('notes' => $order_notes);
		$this->db->where('order_id',$cart_id);
		$query = $this->db->update('orders',$object);

		return $query;

	}

	//REPORTS
	public function topSelling()
	{
		$query = $this->db->query("
			SELECT
				products.description,
				count(order_details.product_id) as num_orders
			FROM
				order_details
			INNER JOIN products
			ON order_details.product_id = products.product_id
			GROUP by order_details.product_id
			ORDER BY num_orders DESC
			");
	}
}

/* End of file Model_orders.php */
/* Location: ./application/models/Model_orders.php */
