<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_auth extends CI_Model {

	public function check_user($username, $password)
		{
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$this->db->where('is_active', 1);
			$query = $this->db->get('users');

			return $query->row_array();
		}

	public function resetPass($id)
		{
			$this->db->where('id', $id);
			$object = array('password' =>md5('Password123'));
			$query = $this->db->update('users', $object);

			return $query;
		}

	public function changePass($id)
		{
			$this->db->where('id', $id);
			$object = array('password' =>md5($this->input->post('password')));
			$query = $this->db->update('users', $object);

			return $query;
		}
}

/* End of file Model_auth.php */
/* Location: ./application/models/Model_auth.php */