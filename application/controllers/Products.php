<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();

         if(!$this->session->userdata('is_loggedin')) {
            redirect(base_url('auth'));
            }

        }
        

	public function index()
	{
		$data['page_title'] = "Products | Lola Tanang's";
		$data['content'] = "product/product";
		$data['product_list'] = $this->Model_products->get_products();
		$this->load->view('main',$data);
    }
    
    public function category()
    {
        $data['page_title'] = "Category | Lola Tanang's";
		$data['content'] = "product/category";
		$data['category_list'] = $this->Model_products->get_categories();
		$this->load->view('main',$data);
    }

    public function addCategory()
    {
    	$data['page_title'] = "Add Category | Lola Tanang's";
		$data['content'] = "product/AddCategory";
		$this->load->view('main',$data);
    }

    public function addProduct()
    {
    	$data['page_title'] = "Add Product | Lola Tanang's";
		$data['content'] = "product/AddProduct";
		$data['cat_list'] = $this->Model_products->get_categories();
		$this->load->view('main',$data);
    }

    public function saveProduct()
    {
    	$query = $this->Model_products->save_product();
    	if($query){
            $this->session->set_flashdata('save_ok', 'Product details saved.');
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    public function saveCategory()
    {
    	$query = $this->Model_products->save_category();
    	if($query){
            $this->session->set_flashdata('save_ok', 'Category details saved.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function editProduct($id)
    {
        $data['page_title'] = "Edit Product | Lola Tanang's";
        $data['content'] = "product/EditProduct";
        $data['prod_details'] = $this->Model_products->product_details($id);
        $data['category_list'] = $this->Model_products->get_categories();
        $this->load->view('main',$data);
    }

    public function updateProduct()
    {
        $result = $this->Model_products->updateProduct($this->input->post('prod_id'));
        if($result){
            $this->session->set_flashdata('update_ok', 'Product details updated.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function deleteProduct($id)
    {
        $result = $this->Model_products->deleteProduct($id);

        if($result){
            $this->session->set_flashdata('delete_ok', 'Product deleted.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function editCategory($id)
    {
        $data['page_title'] = "Edit Category | Lola Tanang's";
        $data['content'] = "product/EditCategory";
        $data['cat_details'] = $this->Model_products->category_details($id);
        $this->load->view('main',$data);
    }

    public function updateCategory()
    {
        $result = $this->Model_products->updateCategory($this->input->post('cat_id'));
        if($result){
            $this->session->set_flashdata('cat_update', 'Category details updated.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function deleteCategory($id)
    {
        $result = $this->Model_products->category_used($id);

        if($result){
            $this->session->set_flashdata('del_err', 'Unable to delete, category is being used.');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
             $result = $this->Model_products->deleteCategory($id);

            if($result){
                $this->session->set_flashdata('del_cat', 'Category deleted.');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function addProductPicture($id)
    {
        $data['page_title'] = "Edit Category | Lola Tanang's";
        $data['product_details'] = $this->Model_products->product_details($id);
        $data['content'] = "product/UploadImage";
        $this->load->view('main',$data);
    }

}

/* End of file Products.php */
/* Location: ./application/controllers/Products.php */