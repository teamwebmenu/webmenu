<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		$this->load->view('auth/login');
	}

	public function validate()
		{

			$query = $this->Model_auth->check_user($this->input->post('signin_username'), md5($this->input->post('signin_password')));


			if($query !=NULL){

				$session_data = array(
			 	 	 	'is_loggedin' => TRUE,
			 	 	 	'user_id' => $query['id'],
			 	 	 	'user_name' =>  $query['fname'],
			 	 	 	'full_name' =>  $query['fname']." ".$query['lname'],
			 	 	 	'access_level' => $query['access_level'],
			 	 	 	'user_image' => $query['image']
			 	 	 );
			 	$this->session->set_userdata($session_data);
				redirect(base_url('dashboard'));
			}else{
				$this->session->set_flashdata('err_msg', 'Invalid username or password.');
				redirect(base_url('auth'));
			}


		}

	public function logout()
		{
			$this->session->sess_destroy();
			redirect(base_url('auth'),'refresh');
		}


}
