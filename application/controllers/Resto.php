<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resto extends CI_Controller {

	public function index()
	{
		$data['page_title'] = "WebMenu | Lola Tanang's";
		$data['food_items'] = $this->Model_products->get_products();
		$data['content'] = "resto/contents/index";
		$this->load->view('resto/main',$data);
	}

	public function menu()
	{
		$data['page_title'] = "WebMenu | Lola Tanang's";
		$data['content'] = "resto/contents/menu";
		$this->load->view('resto/main',$data);
	}

	public function tor()
	{
		$data['page_title'] = "TOR | Lola Tanang's";
		$data['content'] = "resto/contents/terms_and_conditions";
		$this->load->view('resto/main',$data);
	}

}

/* End of file Resto.php */
/* Location: ./application/controllers/Resto.php */