<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImageUpload extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url', 'form');
    }


   public function do_upload() { 

        $product_id = $this->input->post('product_id');
        $product_name = $this->input->post('product_name');

         $config['file_name'] = strtolower($product_id);

         $config['upload_path']   = './food_items/'; 
         $config['allowed_types'] = 'jpg|png'; 
         $config['max_size']      = 1000;  
         $config['overwrite'] = TRUE;

         $this->load->library('upload', $config);
         $this->upload->initialize($config);
            
         if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors()); 
            $this->session->set_flashdata('upload_failed', 'Upload failed. ' . $error['error']);
            redirect($_SERVER['HTTP_REFERER']);
         }
            
         else { 
            $data = array('upload_data' => $this->upload->data()); 
            $upload_success = $this->Model_products->updateProductImg($product_id,$product_id);
            if($upload_success){
                $this->session->set_flashdata('upload_ok', 'Product image uploaded.');
                redirect($_SERVER['HTTP_REFERER']);
            }
         } 
      } 


   public function propic_upload() { 

        $user_id = $this->session->userdata('user_id');
        //$product_name = $this->input->post('product_name');

         $config['file_name'] = strtolower($user_id);

         $config['upload_path']   = './user_pic/'; 
         $config['allowed_types'] = 'jpg'; 
         $config['max_size']      = 1000;  
         $config['overwrite'] = TRUE;

         $this->load->library('upload', $config);
         $this->upload->initialize($config);
            
         if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors()); 
            $this->session->set_flashdata('upload_failed', 'Upload failed. ' . $error['error']);
            redirect($_SERVER['HTTP_REFERER']);
         }
            
         else { 
            $data = array('upload_data' => $this->upload->data()); 
            $upload_success = $this->Model_users->updateUserImg($user_id,$user_id);
            if($upload_success){
                $this->session->set_flashdata('upload_ok', 'Profile image uploaded.');
                redirect($_SERVER['HTTP_REFERER']);
            }
         } 
      } 

}

/* End of file ImageUpload.php */
/* Location: ./application/controllers/ImageUpload.php */