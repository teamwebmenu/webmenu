<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct() {
		parent::__construct();

		 if(!$this->session->userdata('is_loggedin')) {
        	redirect(base_url('auth'));
        	}

		}
		

	public function index()
	{
		redirect('settings/users');
	}

    public function users()
    { //User List
        $data['page_title'] = "WebMenu | Lola Tanang's";
		$data['content'] = "system/users";
		$data['user_list'] = $this->Model_users->get_users();
		$this->load->view('main',$data);
    }

	
	public function profile()
	{
		$data['page_title'] = "WebMenu | Lola Tanang's";
		$data['content'] = "system/profile";
		$this->load->view('main',$data);
	}

	public function changePassword()
	{
		$data['page_title'] = "WebMenu | Lola Tanang's";
		$data['content'] = "system/changePassword";
		$this->load->view('main',$data);
	}

	public function addUser()
	{
		$data['page_title'] = "WebMenu | Lola Tanang's";
		$data['content'] = "system/AddUser";
		$this->load->view('main',$data);
	}

	public function saveUser()
	{
		$result = $this->Model_users->saveUser();

		if($result){
            $this->session->set_flashdata('save_ok', 'New user saved.');
            redirect($_SERVER['HTTP_REFERER']);
        }
	}

	public function editUser($id)
	{
		$data['page_title'] = "WebMenu | Lola Tanang's";
		$data['content'] = "system/EditUser";
		$data['user_info'] = $this->Model_users->user_details($id);
		$this->load->view('main',$data);

	}

	public function updateUser()
	{
		$result = $this->Model_users->updateUser($this->input->post('id'));

		if($result){
            $this->session->set_flashdata('update_ok', 'User details updated.');
            redirect($_SERVER['HTTP_REFERER']);
        }

	}

	public function disableUser($id)
	{
		$result = $this->Model_users->isActive($id);

		if($result["is_active"]==1)
		{
			$response = $this->Model_users->disableAccount($id);
			if($response){
            	$this->session->set_flashdata('disable_ok', 'User account disabled.');
           		redirect($_SERVER['HTTP_REFERER']);
        	}
		}else{
			$response = $this->Model_users->enableAccount($id);
			if($response){
            	$this->session->set_flashdata('enable_ok', 'User account enabled.');
           		redirect($_SERVER['HTTP_REFERER']);
        	}
		}
		
	}

	public function resetPass($id)
	{
		$result = $this->Model_auth->resetPass($id);

		if($result){
            $this->session->set_flashdata('resetpass', 'Account password resetted.');
            redirect($_SERVER['HTTP_REFERER']);
        }
	}

	public function changePass($id)
	{
		$result = $this->Model_auth->changePass($id);

		if($result){
            $this->session->set_flashdata('change_ok', 'Account password changed.');
            redirect($_SERVER['HTTP_REFERER']);
        }
	}

	public function imageUpload()//upload user profile picture
	{
		$data['page_title'] = "Image Upload | Lola Tanang's";
		$data['content'] = "system/ImageUpload";
		$this->load->view('main',$data);
	}
	
	
}
