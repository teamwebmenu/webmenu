<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct() {
		parent::__construct();

		 if(!$this->session->userdata('is_loggedin')) {
        	redirect(base_url('auth'));
        	}

		}

	public function index()
	{
		
		if($this->session->userdata('access_level')==2){
			redirect('dashboard','refresh');
		}else{
			$data['page_title'] = "WebMenu | Lola Tanang's";
			$data['content'] = "order/Order";
			$data['order_list'] = $this->Model_orders->listOrders();
			$this->load->view('main',$data);
		}
	}

	public function selectOrder()
	{
		$data['page_title'] = "Order Cart | Lola Tanang's";
		$data['content'] = "order/Cart";
		$data['prod_list'] = $this->Model_products->get_products();
		$this->load->view('main',$data);
	}

	public function addToCart($id)
	{
		if(!$this->session->userdata('current_cart')){
		 	$new_cart_id =  $this->session->userdata('user_id') . mt_rand();
		 	$array = array(
		 		'current_cart' => $new_cart_id
		 	);

		 	
		 	$this->session->set_userdata($array);
		 	
		 	$query1 = $this->createOrder($this->session->userdata('current_cart'));
		 	$query2 = $this->addItemToCart($this->session->userdata('current_cart'));

		 	if($query1 && $query2){
	            $this->session->set_flashdata('cart_ok', 'New Order Cart created '. $this->session->userdata('current_cart'));
	            redirect($_SERVER['HTTP_REFERER']);
	        }

		 }else{
		 	$query = $this->addItemToCart($this->session->userdata('current_cart'));
		 	if($query){
	            $this->session->set_flashdata('cart_ok', 'Item added to cart '. $this->session->userdata('current_cart'));
	            redirect($_SERVER['HTTP_REFERER']);
	        }
		 }

	}

	function createOrder($cart_id)
	{
		$data = array(
			'order_id' => $this->session->userdata('current_cart'), 
			'user_id' => $this->session->userdata('user_id'),
			'is_paid' => 0
		);
		$this->db->where('order_id', $cart_id);
		$query = $this->db->insert('orders', $data);
		return $query;
	}

	function addItemToCart($cart_id)
	{
		$data = array(
			'order_id' => $this->session->userdata('current_cart'), 
			'product_id' => $this->input->post('product_id'),
			'quantity' => $this->input->post('quantity'),
			'unit_price' => $this->input->post('unit_price')
		);

		$query = $this->db->insert('order_details', $data);
		return $query;
	}

	public function clearCartSession()
	{
		$this->session->unset_userdata('current_cart');
	}

	public function myOrder($user_id)
	{
		$data['page_title'] = "My Cart | Lola Tanang's";
		$data['content'] = "order/my_order";
		$data['my_orders'] = $this->Model_orders->my_order($user_id);
		$this->load->view('main',$data);
	}

	public function myOrderDetails($order_id)
	{
		$data['page_title'] = "My Cart Details | Lola Tanang's";
		$data['content'] = "order/my_order_details";
		$data['order_id'] = $order_id;
		$data['my_orders'] = $this->Model_orders->my_order_details($order_id);
		$this->load->view('main',$data);

	}

	//CUSTOME ORDER ACTIONS
	public function removeItem($id)
	{
		$result = $this->Model_orders->removeItem($id);
		
		if($result){
	        $this->session->set_flashdata('remove_ok', 'Item has been removed.');
	        redirect($_SERVER['HTTP_REFERER']);
	    }
	}

	public function delete_order($id)
	{
		$query = $this->Model_orders->delete_order($id);
		$this->session->unset_userdata('current_cart');

		if($query){
	        $this->session->set_flashdata('delete_ok', 'Your order has been cancelled.');
	        redirect($_SERVER['HTTP_REFERER']);
	    }

	}

	public function addItems_this_Cart($cart_id)
	{
		$this->session->set_userdata('current_cart',$cart_id);
		redirect(base_url('dashboard'),'refresh');

	}

	public function newOrder()
	{
		$this->session->unset_userdata('current_cart');
		redirect(base_url('dashboard'),'refresh');

	}

	public function submitOrder(){

		$order_notes = $this->input->post("order_notes");
		$cart_id = $this->input->post('cart_id');

		$query = $this->Model_orders->submitOrder($cart_id);
		$orderNote = $this->Model_orders->addOrderNotes($cart_id,$order_notes);

		$this->session->unset_userdata('current_cart');
		
		if($query){
	        $this->session->set_flashdata('submit_ok', 'Your order has been submitted.');
	         redirect($_SERVER['HTTP_REFERER']);
	    }else{
	    	echo 'error submitting order';
	    }
	}

	public function acknowledgeOrder($cart_id){

		$query = $this->Model_orders->acknowledgeOrder($cart_id);

		if($query){
	     $this->session->set_flashdata('ack_ok', 'Order has been acknowledged.');
	     redirect($_SERVER['HTTP_REFERER']);
	 	}
	}

	public function readyOrder($cart_id){

		$query = $this->Model_orders->readyOrder($cart_id);

		if($query){
	     $this->session->set_flashdata('ack_ok', 'Order is ready for pickup.');
	     redirect($_SERVER['HTTP_REFERER']);
	 	}
	}

	public function payOrder($cart_id){

		$query = $this->Model_orders->payOrder($cart_id);

		if($query){
	     $this->session->set_flashdata('ack_ok', 'Order is paid.');
	     redirect($_SERVER['HTTP_REFERER']);
	 	}
	}

	public function viewReceipt($cart_id){

		$data['page_title'] = "Invoice | Lola Tanang's";
		$data['invoice_header'] = $this->Model_orders->invoiceHeader($cart_id);
		$data['invoice_details'] = $this->Model_orders->my_order_details($cart_id);
		$this->load->view('order/Invoice',$data);
	}



}
