<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct() {
		parent::__construct();

		 if(!$this->session->userdata('is_loggedin')) {
        	redirect(base_url('auth'));
        	}

		}

	public function index()
	{
		
	}

	public function topProducts()
	{
		$data['page_title'] = "Top Products | Lola Tanang's";
		$data['content'] = "reports/TopSellingProduct";
		$this->load->view('main',$data);
	}

	public function monthlySales()
	{
		$data['page_title'] = "Monthly Sales | Lola Tanang's";
		$data['content'] = "reports/MonthlySalesReport";
		$this->load->view('main',$data);
	}


}

/* End of file Reports.php */
/* Location: ./application/controllers/Reports.php */