<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();

		 if(!$this->session->userdata('is_loggedin')) {
        	redirect(base_url('auth'));
        	}

		}


	public function index()
	{
		$user_mode = $this->session->userdata('access_level');
		$data['page_title'] = "WebMenu | Lola Tanang's";
		if($user_mode==2)
		{
			$data['food_items'] = $this->Model_products->get_products();
			$data['content'] = "dashboard/dashboard_customer";
		}else{
			$data['total_sales'] =  $this->Model_orders->get_total_sales();
			$data['order_count'] = $this->Model_orders->count_orders();
			$data['pending'] = $this->Model_orders->pending_orders();
			$data['cancelled'] = $this->Model_orders->cancelled_orders();
			$data['paid'] = $this->Model_orders->paid_orders();
			$data['content'] = "dashboard/dashboard";
		}
		
		$this->load->view('main',$data);


	}

	public function logout()
	{
		$this->load->view('auth/login');
	}
}
