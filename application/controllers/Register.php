<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function index()
	{
		$this->load->helper('captcha');
		$this->load->view('register/registration');

	}

	public function new_customer()
	{
		if($this->input->post("gen_captcha")==$this->input->post("captcha")){

				$check_exists = $this->Model_users->customer_exists($this->input->post("mobile_number"));

				if($check_exists){
					$this->session->set_flashdata('customer_exists', 'Registered number already exists.');
		            redirect($_SERVER['HTTP_REFERER']);
				}else{
					$query = $this->Model_users->add_customer();
			    	if($query){
			            $this->session->set_flashdata('customer_ok', 'You are now registered. Please login to continue.');
			            redirect($_SERVER['HTTP_REFERER']);
			        }
				}
			}else{
				$this->session->set_flashdata('invalid_captcha', 'Invalid Security Key.');
		         redirect($_SERVER['HTTP_REFERER']);
			}//captchat condition
	}//function

}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */