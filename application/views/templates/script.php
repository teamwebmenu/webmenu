<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
  <script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>theme/assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
  <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->

<script src="<?php echo base_url();?>theme/assets/javascripts/jquery.transit.js"></script>

<!-- Pixel Admin's javascripts -->
<script src="<?php echo base_url();?>theme/assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/javascripts/pixel-admin.min.js"></script>

<script type="text/javascript">
  init.push(function () {
    // Javascript code here
  });
  window.PixelAdmin.start(init);
</script>