  <div id="main-menu" role="navigation">
    <div id="main-menu-inner">
      <div class="menu-content top" id="menu-content-demo">
        <div>
          <div class="text-bg"><span class="text-slim">Welcome,</span> <span class="text-semibold"><?php echo $this->session->userdata('user_name'); ?></span></div>

          <img src="<?php echo base_url('user_pic/'.$this->session->userdata('user_image'));?>" alt="" class="">
          <div class="btn-group">
            <!-- <a href="<?php echo base_url('dashboard');?>" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-envelope"></i></a>
            <a href="<?php echo base_url('dashboard');?>" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-user"></i></a>
            <a href="<?php echo base_url('dashboard');?>" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-cog"></i></a> -->
<!--             <a href="<?php echo base_url('auth/logout');?>" class="btn btn-xs btn-danger btn-outline dark"><i class="fa fa-power-off"></i></a>
 -->          </div>
        </div>
      </div>
      <ul class="navigation">
        <li>
          <a href="<?php echo base_url('dashboard');?>"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a>
        </li>
        <?php if($this->session->userdata('access_level')==2){ ?>
          <li>
            <a href="<?php echo base_url('order/myOrder/'.$this->session->userdata('user_id'));?>"><i class="menu-icon fa fa-tasks"></i><span class="mm-text">My Order</span></a>
          </li>
        <?php } ?>

         <?php if($this->session->userdata('access_level')==1){ ?>
          <!-- <li>
          <a href="<?php echo base_url('order/selectOrder');?>"><i class="menu-icon fa fa-tasks"></i><span class="mm-text">Order Menu</span></a>
        </li> -->

        <li>
          <a href="<?php echo base_url('order');?>"><i class="menu-icon fa fa-tasks"></i><span class="mm-text">Order Management</span></a>
        </li>

        <li class="mm-dropdown">
          <a href="#"><i class="menu-icon fa fa-shopping-cart"></i><span class="mm-text">Product Management</span></a>
          <ul>
            <li>
              <a tabindex="-1" href="<?php echo base_url('products');?>"><span class="mm-text">Product List</span></a>
            </li>
            <li>
              <a tabindex="-2" href="<?php echo base_url('products/category');?>"><span class="mm-text">Category List</span></a>
            </li>
          </ul>
        </li>
        

        <li class="mm-dropdown">
          <a href="#"><i class="menu-icon fa fa-bar-chart-o"></i><span class="mm-text">Reports</span></a>
          <ul>
            <li>
              <a tabindex="-1" href="<?php echo base_url('reports/topProducts');?>"><span class="mm-text">Top Selling Proudct</span></a>
            </li>
            <li>
              <a tabindex="-2" href="<?php echo base_url('reports/monthlySales');?>"><span class="mm-text">Monthly Sales Report</span></a>
            </li>
          </ul>
        </li>

        <?php }?>
        
        <li class="mm-dropdown">
          <a href="pages-blank.html#"><i class="menu-icon fa fa-wrench"></i><span class="mm-text">Settings</span></a>
          <ul>
            <!-- <li>
              <a tabindex="-1" href="<?php echo base_url('settings/profile');?>"><span class="mm-text">My Profile</span></a>
            </li> -->
            <li>
              <a tabindex="-2" href="<?php echo base_url('settings/changePassword');?>"><span class="mm-text">Change Password</span></a>
            </li>

            <?php if($this->session->userdata('access_level')==1){ ?>
              <li>
                <a tabindex="-3" href="<?php echo base_url('settings/users');?>"><span class="mm-text">User Accounts</span></a>
              </li>
            <?php }?>

          </ul>
        </li>
        <!-- <li>
          <a href="tables.html"><i class="menu-icon fa fa-table"></i><span class="mm-text">Tables</span></a>
        </li>
        <li>
          <a href="charts.html"><i class="menu-icon fa fa-bar-chart-o"></i><span class="mm-text">Charts</span></a>
        </li>
        
        <li>
          <a href="complete-ui.html"><i class="menu-icon fa fa-briefcase"></i><span class="mm-text">Complete UI</span></a>
        </li>
        <li>
          <a href="color-builder.html"><i class="menu-icon fa fa-tint"></i><span class="mm-text">Color Builder</span></a>
        </li> -->
      
      </ul> 
      <div class="menu-content">
        <a href="<?php echo base_url('auth/logout');?> " class="btn btn-primary btn-block btn-outline dark">Log Out</a>
      </div>
    </div> 
  </div> 
