  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<!-- Open Sans font from Google CDN -->
  <link href="<?php echo base_url();?>theme/assets/stylesheets/google_font.css" rel="stylesheet" type="text/css">

  <!-- Pixel Admin's stylesheets -->
  <link href="<?php echo base_url();?>theme/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>theme/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>theme/assets/stylesheets/widgets.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>theme/assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>theme/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>theme/assets/stylesheets/themes.min.css" rel="stylesheet" type="text/css">

  <!--[if lt IE 9]>
    <script src="assets/javascripts/ie.min.js"></script>
  <![endif]-->