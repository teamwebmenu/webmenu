  <div id="main-navbar" class="navbar navbar-inverse" role="navigation">
    <button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">HIDE MENU</span></button>
    
    <div class="navbar-inner">
      <div class="navbar-header">

        <!-- Logo -->
        <a href="<?php echo base_url('dashboard');?>" class="navbar-brand">
          <div><img alt="Pixel Admin" src="<?php echo base_url();?>/theme/assets/images/pixel-admin/main-navbar-logo.png"></div>
          Lola Tanang's
        </a>

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse"><i class="navbar-icon fa fa-bars"></i></button>

      </div> 
      <div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
        <div>
          <ul class="nav navbar-nav">
            <!-- <li>
              <a href="<?php echo base_url();?>">Home</a>
            </li> -->
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown</a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('dashboard');?>">First item</a></li>
                <li><a href="<?php echo base_url('dashboard');?>">Second item</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url('dashboard');?>">Third item</a></li>
              </ul>
            </li> -->
          </ul> 
          <div class="right clearfix">
            <ul class="nav navbar-nav pull-right right-navbar-nav">

             <!-- <li class="nav-icon-btn nav-icon-btn-danger dropdown">
                <a href="pages-blank.html#notifications" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="label">5</span>
                  <i class="nav-icon fa fa-bullhorn"></i>
                  <span class="small-screen-text">Notifications</span>
                </a>

                
                <script>
                  init.push(function () {
                    $('#main-navbar-notifications').slimScroll({ height: 250 });
                  });
                </script>

                 <div class="dropdown-menu widget-notifications no-padding" style="width: 300px">
                  <div class="notifications-list" id="main-navbar-notifications">

                    <div class="notification">
                      <div class="notification-title text-danger">SYSTEM</div>
                      <div class="notification-description"><strong>Error 500</strong>: Syntax error in index.php at line <strong>461</strong>.</div>
                      <div class="notification-ago">12h ago</div>
                      <div class="notification-icon fa fa-hdd-o bg-danger"></div>
                    </div> 

                  </div> 
                  <a href="#" class="notifications-link">MORE NOTIFICATIONS</a>
                </div>  
              </li>-->
             <!--  <li class="nav-icon-btn nav-icon-btn-success dropdown">
                <a href="http://infinite-woodland-5276.herokuapp.com/mail.ru" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="label">10</span>
                  <i class="nav-icon fa fa-envelope"></i>
                  <span class="small-screen-text">Income messages</span>
                </a>

               
                <script>
                  init.push(function () {
                    $('#main-navbar-messages').slimScroll({ height: 250 });
                  });
                </script> -->

               <!--  <div class="dropdown-menu widget-messages-alt no-padding" style="width: 300px;">
                  <div class="messages-list" id="main-navbar-messages">

                    <div class="message">
                      <img src="assets/demo/avatars/2.jpg" alt="" class="message-avatar">
                      <a href="#" class="message-subject">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a>
                      <div class="message-description">
                        from <a href="pages-blank.html#">Robert Jang</a>
                        &nbsp;&nbsp;·&nbsp;&nbsp;
                        2h ago
                      </div>
                    </div> 
                  </div> 
                  <a href="#" class="messages-link">MORE MESSAGES</a>
                </div> 
              </li>
--> 
             <!--  <li>
                <form class="navbar-form pull-left">
                  <input type="text" class="form-control" placeholder="Search">
                </form>
              </li> -->

              <li class="dropdown">
                <a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
                  <img src="<?php echo base_url('user_pic/'.$this->session->userdata('user_image'));?>" alt="">
                  <span><?php echo $this->session->userdata('full_name'); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- <li><a href="<?php echo base_url('settings/profile');?>"><i class="dropdown-icon fa fa-user"></i>&nbsp;&nbsp;My Profile</a></li> -->
                  <li><a href="<?php echo base_url('settings/changePassword');?>"><i class="dropdown-icon fa fa-wrench"></i>&nbsp;&nbsp;Change Password</a></li>
                  <li><a href="<?php echo base_url('settings/imageUpload');?>"><i class="dropdown-icon fa fa-camera"></i>&nbsp;&nbsp;Upload Image</a></li>
                  <!-- <li><a href="#"><i class="dropdown-icon fa fa-cog"></i>&nbsp;&nbsp;Settings</a></li> -->
                  <li class="divider"></li>
                  <li><a href="<?php echo base_url('auth/logout');?>"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
                </ul>
              </li>
            </ul> 
          </div> 
        </div>
      </div> 
    </div> 
  </div> 