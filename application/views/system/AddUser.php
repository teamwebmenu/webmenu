			<div class="col-md-6">
				<script>
					init.push(function () {
						
						// Setup validation
						$("#frm_addUser").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'last_name': {
								  required: true,
								},
								'first_name': {
								  required: true,
								},
								'position': {
								  required: true,
								},
								'password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'password_confirm': {
									required: true,
									minlength: 6,
									equalTo: "#password"
								},
								'access_level': {
								  required: true,
								},
							}
						});
					});
				</script>

				<?php if($this->session->flashdata('save_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> You successfully added a new user.
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">User Accounts &rarr; Add User</span>
					</div>
					<div class="panel-body">
						<div class="note note-info">Please provide user account information. The username will be the first letter of the firstname and the last name. Example: John Doe, the username is <strong>jdoe</strong>.</div>
						
						<form class="form-horizontal" id="frm_addUser" method="post" action="<?php echo base_url('settings/saveUser');?>">
							<div class="form-group dark">
								<label for="last_name" class="col-sm-3 control-label">Last Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
								</div>
							</div>

							<div class="form-group dark">
								<label for="first_name" class="col-sm-3 control-label">First Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
								</div>
							</div>

							<div class="form-group dark">
								<label for="position" class="col-sm-3 control-label">Position</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="position" name="position" placeholder="Position">
								</div>
							</div>

							<div class="form-group dark">
								<label for="password" class="col-sm-3 control-label">Password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password" name="password" placeholder="Password">
								</div>
							</div>

							<div class="form-group dark">
								<label for="password_confirm" class="col-sm-3 control-label">Confirm password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm password">
								</div>
							</div>

							<div class="form-group dark">
								<label for="access_level" class="col-sm-3 control-label">Access Level</label>
								<div class="col-sm-9">
									<select class="form-control" id="access_level" name="access_level">
										<option value="">Select Access Level</option>
										<option value="1">Administrator</option>
										<option value="2">User</option>
									</select>
								</div>
							</div>

							<hr class="panel-wide">

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary btn-rounded">Save User Information</button>
									<a href="<?php echo base_url('settings/users');?>" class="btn btn-warning btn-rounded">Cancel</a>
								</div>

							</div>
						</form>
					</div>
				</div>
		</div>