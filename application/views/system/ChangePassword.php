			<div class="col-md-6">
				<script>
					init.push(function () {
						
						// Setup validation
						$("#frm_ChangePassword").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								
								'password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'password_confirm': {
									required: true,
									minlength: 6,
									equalTo: "#password"
								},
							}
						});
					});
				</script>


				<?php if($this->session->flashdata('change_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('change_ok'); ?>
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">User Accounts &rarr; Change Password</span>
					</div>
					<div class="panel-body">
						<div class="note note-info">Please input your new password.</div>
						
						<form class="form-horizontal" id="frm_ChangePassword" method="post" action="<?php echo base_url('settings/changePass/'. $this->session->userdata('user_id'));?>">
							
							<div class="form-group dark">
								<label for="password" class="col-sm-3 control-label">Password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password" name="password" placeholder="Password">
								</div>
							</div>

							<div class="form-group dark">
								<label for="password_confirm" class="col-sm-3 control-label">Confirm password</label>
								<div class="col-sm-9">
									<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm password">
								</div>
							</div>

							<hr class="panel-wide">

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary btn-rounded">Save New Password</button>
									<a href="<?php echo base_url('settings/users');?>" class="btn btn-warning btn-rounded">Cancel</a>
								</div>

							</div>
						</form>
					</div>
				</div>
		</div>