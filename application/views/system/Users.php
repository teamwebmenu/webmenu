				<script>
					init.push(function () {
						$('#users-table').dataTable();
						$('#users-table_wrapper .table-caption').text('User List');
						$('#users-table_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
					});

				</script>

				<?php if($this->session->flashdata('disable_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('disable_ok'); ?>
					</div>
				<?php } ?>

				<?php if($this->session->flashdata('enable_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('enable_ok'); ?>
					</div>
				<?php } ?>

				
				<?php if($this->session->flashdata('resetpass')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('resetpass'); ?>
					</div>
				<?php } ?>




				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">User Accounts
							<a href="<?php echo base_url('settings/addUser');?>" class="btn btn-sm btn-primary btn-rounded pull-right">Add User</a>
						</span>
					</div>
					<div class="panel-body">
						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="users-table">
								<thead>
									<tr>
										<th>USER ID</th>
										<th>PICTURE</th>
										<th>USERNAME</th>
										<th>FULL NAME</th>
										<th>POSITION</th>
										<th>ACCESS LEVEL</th>
										<th>STATUS</th>
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($user_list as $rows){?>
									<tr class="odd gradeX">
										<td><?=$rows->id?></td>
										<td>
											<img src="<?php echo base_url()."user_pic/".$rows->image;?>" height="80;" width="80;">
										</td>
										<td><?=$rows->username?></td>
										<td><?=$rows->lname.', '.$rows->fname?></td>
										<td><?=$rows->position?></td>
										<td><?php echo ($rows->access_level==1)? 'Administrator':'User' ?></td>
										<td><span class="badge <?php echo ($rows->is_active==1)? 'badge-success':'badge-danger' ?>"><?php  echo ($rows->is_active==1)? 'Active':'In-active' ?></span></td>
										<td class="center">
											<a href="<?php echo base_url('settings/editUser/'.$rows->id);?>" class="btn btn-sm btn-success">Edit</a>

											<a class="btn btn-sm btn-info" data-toggle="modal" data-target="#reset_pass<?=$rows->id?>">Reset</a>

											<a class="btn btn-sm <?php echo ($rows->is_active==1)? 'btn-warning':'btn-gray' ?>" data-toggle="modal" data-target="#disable_user<?=$rows->id?>"><?php echo ($rows->is_active==1)? 'Disable':'Enable' ?></a>



											<!--DISABLE USER -->

											<div id="disable_user<?=$rows->id?>" class="modal modal-alert modal-warning fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<i class="fa <?php echo ($rows->is_active==1)? 'fa-lock':'fa-unlock' ?>"></i>
														</div>
														<div class="modal-title">You are about to <?php echo ($rows->is_active==1)? 'Disable':'Enable' ?> the user account of </br> <?=$rows->lname.', '.$rows->fname?></div>
														<!-- <div class="modal-body">Click outside of this box to cancel.</div> -->
														<div class="modal-footer">
															<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
															<a class="btn btn-warning" href="<?php echo base_url('settings/disableUser/'.$rows->id);?>"><?php echo ($rows->is_active==1)? 'Disable':'Enable' ?></a>
														</div>
													</div> 
												</div> 
											</div> 

											<!--RESET PASSSWORD -->

											<div id="reset_pass<?=$rows->id?>" class="modal modal-alert modal-info fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<i class="fa fa-key"></i>
														</div>
														<div class="modal-title">You are about to reset the password of </br> <?=$rows->lname.', '.$rows->fname?></div>
														<div class="modal-body">Temporary Password is </br>Password123</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
															<a class="btn btn-info" href="<?php echo base_url('settings/resetPass/'.$rows->id);?>">Reset</a>
														</div>
													</div> 
												</div> 
											</div> 


										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>