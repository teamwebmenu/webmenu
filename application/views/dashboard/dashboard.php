<div class="col-md-4">
	<div class="col-sm-4 col-md-12">
		<div class="stat-panel">
			<div class="stat-cell bg-primary valign-middle">
				<i class="fa fa-trophy bg-icon"></i>
				<?php
					$sales = 0;
					foreach ($total_sales as $row) {
					$tot = $row->total_amount; 
					$sales = $sales + $tot;
					}
				?>
				<span class="text-xlg"><span class="text-lg text-slim">Php</span><strong><?php echo number_format($sales,2); ?></strong></span><br>
				<span class="text-bg">Total Sales</span><br>
			</div> 
		</div> 
	</div>
</div>

<div class="col-md-4">
	<div class="col-sm-4 col-md-12">
		<div class="stat-panel">
			<div class="stat-cell bg-info valign-middle">
				<i class="fa fa-shopping-cart bg-icon"></i>
				<?php 
					$count = 0;
					foreach ($order_count as $count) {
						$count = $count->count_orders;
					}
				?>
				<span class="text-xlg"><span class="text-lg text-slim"></span><strong><?php echo $count; ?></strong></span><br>
				<span class="text-bg">Total Orders</span><br>
			</div> 
		</div> 
	</div>
</div>

<div class="col-md-4">
	<div class="col-sm-4 col-md-12">
		<div class="stat-panel">
			<div class="stat-cell bg-success valign-middle">
				<i class="fa fa-money bg-icon"></i>
				<?php 
					$count = 0;
					foreach ($paid as $count) {
						$count = $count->count_orders;
					}
				?>
				<span class="text-xlg"><span class="text-lg text-slim"></span><strong><?php echo $count; ?></strong></span><br>
				<span class="text-bg">Paid Orders</span><br>
			</div> 
		</div> 
	</div>
</div>


<div class="col-md-4">
	<div class="col-sm-4 col-md-12">
		<div class="stat-panel">
			<div class="stat-cell bg-warning valign-middle">
				<i class="fa  fa-exclamation-circle bg-icon"></i>
				<?php 
					$count = 0;
					foreach ($pending as $count) {
						$count = $count->count_orders;
					}
				?>
				<span class="text-xlg"><span class="text-lg text-slim"></span><strong><?php echo $count; ?></strong></span><br>
				<span class="text-bg">Pending Orders</span><br>
			</div> 
		</div> 
	</div>
</div>

<div class="col-md-4">
	<div class="col-sm-4 col-md-12">
		<div class="stat-panel">
			<div class="stat-cell bg-danger valign-middle">
				<i class="fa fa-trash-o bg-icon"></i>
				<?php 
					$count = 0;
					foreach ($cancelled as $count) {
						$count = $count->count_orders;
					}
				?>
				<span class="text-xlg"><span class="text-lg text-slim"></span><strong><?php echo $count; ?></strong></span><br>
				<span class="text-bg">Cancelled Orders</span><br>
			</div> 
		</div> 
	</div>
</div>
