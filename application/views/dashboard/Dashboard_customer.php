<div class="col-md-12">
	<h1>Food Menu</h1>
</div>

<div class="col-md-12">
		<?php if($this->session->flashdata('cart_ok')){?>
		<div class="alert alert-success dark">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>Well done!</strong> <?php echo $this->session->flashdata('cart_ok'); ?>
		</div>
	<?php } ?>
</div>

<?php foreach ($food_items as $food) { ?>

<div class="col-md-3">

	<div class="col-sm-4 col-md-12">
		<div class="stat-panel alert alert-info">
			<div class="stat-cell valign-middle">
				<div style="display: flex; justify-content: center;">
					<img src="<?php echo base_url();?>food_items/<?=$food->upload_image?>" width="120px;" height="100px;">
				</div>
				<form method="post" class="form-horizontal" action="<?php echo base_url('order/addToCart/'. $food->product_id);?>">
						<div class="form-group">
							<span class="text-xlg"><span class="text-lg text-slim">Php </span><strong><?=number_format($food->unit_price,2)?></strong></span><br><hr>
							<span class="text-bg"><?=$food->description?></span><br>
							<span class="text-slim"><?=$food->prod_desc?></span><br>
							<div style="padding-top: 10px;"></div>
						</div>
						<div class="form-group">
							<input type="hidden" name="product_id" value="<?=$food->product_id?>">
							<input type="hidden" name="unit_price" value="<?=$food->unit_price?>">
							<label for="quantity">Qty:</label>
							<input type="number" name="quantity" id="quantity" value="" min="1" required>
						</div>
						<input class="btn btn-sm btn-success btn-rounded" type="submit" name="submit" value="Add to Order">
				</form>
			</div> 
		</div> 
	</div>
</div>

<?php } ?>