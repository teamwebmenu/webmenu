
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Sign In - Lola Tanang's</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  <!-- Open Sans font from Google CDN -->
  <link href="<?php echo base_url();?>theme/assets/stylesheets/google_font.css" rel="stylesheet" type="text/css">

  <!-- Pixel Admin's stylesheets -->
  <link href="<?php echo base_url('theme/');?>assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('theme/');?>assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('theme/');?>assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('theme/');?>assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('theme/');?>assets/stylesheets/themes.min.css" rel="stylesheet" type="text/css">

  <!--[if lt IE 9]>
    <script src="assets/javascripts/ie.min.js"></script>
  <![endif]-->

</head>
<body class="theme-default page-signin">

  <div id="page-signin-bg">
    <div class="overlay"></div>
    <img src="<?php echo base_url('theme/');?>assets/demo/signin-bg-1.jpg" alt="">
  </div>

  <div class="signin-container" style="padding-top: 100px;">

    <div class="signin-info">
      <a href="<?php echo base_url();?>" class="logo">
        <img src="<?php echo base_url('theme/');?>images/lola_tanangs_logo.jpg" alt="" style="margin-top: -5px;" width="200px;" height="210px;"></br>
        
      </a> 
    <!--   <div class="slogan">
        Local. Food. Delicacy.
      </div>  -->
      <!-- <ul>
        <li><i class="fa fa-sitemap signin-icon"></i> Home-Made Recipes</li>
        <li><i class="fa fa-file-text-o signin-icon"></i> LESS &amp; SCSS source files</li>
        <li><i class="fa fa-outdent signin-icon"></i> RTL direction support</li>
        <li><i class="fa fa-heart signin-icon"></i> Cooked with love</li>
      </ul>  -->
    </div>
 
    <div class="signin-form">
      <form method="post" id="signin-form_id" action="<?php echo base_url('Auth/validate');?>">
        <div class="signin-text">
          <span>Lola Tanang's Restaurant</span>
        </div> 
        <div class="form-group w-icon">
          <input type="text" name="signin_username" id="username_id" class="form-control input-lg" placeholder="Username" required>
          <span class="fa fa-user signin-form-icon"></span>
        </div> 

        <div class="form-group w-icon">
          <input type="password" name="signin_password" id="password_id" class="form-control input-lg" placeholder="Password" required>
          <span class="fa fa-lock signin-form-icon"></span>
        </div> 
        <div class="form-group">
          <span><?php echo $this->session->flashdata('err_msg');?></span>
        </div>
        <div class="form-actions">
          <input type="submit" value="Login" class="signin-btn bg-primary">
          <a href="<?php echo base_url();?>" class="signin-btn bg-warning">Cancel</a>
        </div> 
      </form>
          <p style="padding-top: 20px;">No account yet? Click <a href="<?php echo base_url('register');?>"><strong>here</strong></a> to register.</p>
    </div>

  </div>

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
  <script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>theme/assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
  <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->

<!-- Pixel Admin's javascripts -->
<script src="<?php echo base_url('theme/');?>assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url('theme/');?>assets/javascripts/pixel-admin.min.js"></script>

</body>
</html>

<script>
  function validateForm() {
    alert('working!');
  }
}
</script>