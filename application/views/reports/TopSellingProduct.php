
				<script>
					init.push(function () {
						Morris.Bar({
							element: 'hero-bar',
							data: [
								{ productName: 'Chicken', stat_value: 100 },
								{ productName: 'Fish', stat_value: 200 },
								{ productName: 'Soup', stat_value: 400 },
								{ productName: 'Drinks', stat_value: 300 },
								{ productName: 'Barbeque', stat_value: 500 },
								{ productName: 'Pasta', stat_value: 600 },
							],
							xkey: 'productName',
							ykeys: ['stat_value'],
							labels: ['Total Sales'],
							barRatio: 0.4,
							xLabelAngle: 35,
							hideHover: 'auto',
							barColors: PixelAdmin.settings.consts.COLORS,
							gridLineColor: '#cfcfcf',
							resize: true
						});
					});
				</script>
				<!-- / Javascript -->
				
				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Top 10 Selling Products</span>
					</div>
					<div class="panel-body">
						<!-- <div class="note note-info">More info and examples at <a href="http://www.oesmith.co.uk/morris.js/" target="_blank">http://www.oesmith.co.uk/morris.js/</a></div> -->

						<div class="graph-container">
							<div id="hero-bar" class="graph"></div>
						</div>
					</div>
				</div>
