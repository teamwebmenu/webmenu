
				<script>
					init.push(function () {
						Morris.Bar({
							element: 'hero-bar',
							data: [
								{ monthName: 'January', stat_value: 100 },
								{ monthName: 'February', stat_value: 200 },
								{ monthName: 'March', stat_value: 400 },
								{ monthName: 'April', stat_value: 300 },
								{ monthName: 'May', stat_value: 500 },
								{ monthName: 'June', stat_value: 600 },
								{ monthName: 'July', stat_value: 500 },
								{ monthName: 'August', stat_value: 600 },
								{ monthName: 'September', stat_value: 500 },
								{ monthName: 'October', stat_value: 600 },
								{ monthName: 'November', stat_value: 500 },
								{ monthName: 'December', stat_value: 600 }
							],
							xkey: 'monthName',
							ykeys: ['stat_value'],
							labels: ['Total Sales'],
							barRatio: 0.4,
							xLabelAngle: 35,
							hideHover: 'auto',
							barColors: PixelAdmin.settings.consts.COLORS,
							gridLineColor: '#cfcfcf',
							resize: true
						});
					});
				</script>
				<!-- / Javascript -->
				
				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Monthly Sales Report</span>
					</div>
					<div class="panel-body">
						<!-- <div class="note note-info">More info and examples at <a href="http://www.oesmith.co.uk/morris.js/" target="_blank">http://www.oesmith.co.uk/morris.js/</a></div> -->

						<div class="graph-container">
							<div id="hero-bar" class="graph"></div>
						</div>
					</div>
				</div>
