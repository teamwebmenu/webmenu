<!doctype html>
<html lang="en">

<head>
  <?php $this->load->view('resto/templates/styles'); ?>
</head>

<body>
   <?php $this->load->view('resto/templates/navbar'); ?>

    <?php $this->load->view($content); ?>
    
    <?php $this->load->view('resto/templates/footer'); ?>
     <?php $this->load->view('resto/templates/scripts'); ?>
</body>

</html>