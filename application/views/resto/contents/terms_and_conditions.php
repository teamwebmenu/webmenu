
    <section class="exclusive_item_part blog_item_section" style="padding-top: 120px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-5">
                    <div class="section_tittle">
                        <p>Lola Tanang's Restaurant</p>
                        <h2>Terms and Conditions</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <p>This website is owned by Lola Tanang’s Restaurant, located at Vicentillo Street, Municipality of Naval, Province of Biliran, Philippines.
                By using this website you agree to these terms and conditions. Please read carefully.
                For products and services, term conditions, requirements, and rates may vary from information posted on this website.
                </p>
            </div>
            <div class="row">
                <div class="section_tittle col-md-10" style="padding-top: 20px;">
                    <h3>Copyright</h3>
                    <p>All copyright and other intellectual property contained on this website is owned by or licensed to Lola Tanang’s Restaurant. The information contained in this website may be used for your personal reference only. Information in this website may not otherwise be reproduced, distributed, or transmitted to any other person or entity, or may not in any way be reproduced in any other form.</p>
                </div>
            </div>

             <div class="row">
                <div class="section_tittle col-md-10">
                    <h3>Privacy Policy</h3>
                    <p>The Lola Tanang’s Restaurant privacy policies explain how we shall treat your personal data and protect your privacy when you use our website. By using our website, you agree that Lola Tanang’s Restaurant can use such data in accordance with our privacy policies.</p>
                </div>
            </div>

             <div class="row">
                <div class="section_tittle col-md-10">
                    <h3>Liability</h3>
                    <p>Lola Tanang’s Restaurant is not liable to any person in relation to using this website. You also agree to waive all claims against Lola Tanang’s Restaurant in relation to use of this website.
                    You indemnify Lola Tanang’s Restaurant and its employees from any breach of these terms.
                    </p>
                </div>
            </div>

             <div class="row">
                <div class="section_tittle col-md-10">
                    <h3>What Lola Tanang’s Restaurant Can Collect from You</h3>
                    <p>When you apply or use any product or service Lola Tanang’s Restaurant provides or when you engage with our employees, authorized representatives, agents and providers, we collect your personal information. This may include, among others, your name, signature, and personal details such as contact details, address, date of birth, education; government ID details; financial information (such as income, expense, investment, tax, financial history and transaction, etc.); employment details; business interests and property; images by CCTV and other similar devices installed in our offices.
                    We may, whenever required, prove or supplement this information to third-parties including government regulators, judicial, administrative bodies, tax authorities or competent jurisdictional courts and, in the process, obtain additional information about you.
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="section_tittle col-md-10">
                    <h3>How We Protect Your Information</h3>
                    <p>We fully recognize the value of your personal information which may include sensitive personal information such as your gender, government issued IDs, etc. At the same time, we strive to maintain confidentiality, integrity and availability of your personal information through the use of physical, technological and security techniques. We train our employees to properly handle your information. Whenever we engage with other companies to provide services for us, we ask them to protect personal information that is aligned with our own security standards.
                    </p>
                </div>
            </div>



        </div>
    </section>
    <!--::exclusive_item_part end::-->

   