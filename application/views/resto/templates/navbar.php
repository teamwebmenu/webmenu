    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="<?php echo base_url();?>"> <img src="<?php echo base_url('theme/resto/');?>img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('resto');?>">Home</a>
                                </li> -->
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="#exclusiveItems">Specialties</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#foodMenu">Food Menu</a>
                                </li> -->
                            </ul>
                        </div>
                        <div class="menu_btn">
                            <a href="<?php echo base_url('auth');?>" class="btn_1 d-none d-sm-block">Login / Register</a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>