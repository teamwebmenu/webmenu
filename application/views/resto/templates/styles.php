   <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lola Tanang's Restaurant </title>
    <link rel="icon" href="<?php echo base_url('theme/resto/');?>img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/gijgo.min.css">
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/nice-select.css">
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="<?php echo base_url('theme/resto/');?>css/style.css">