<script src="<?php echo base_url('theme/resto/');?>js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="<?php echo base_url('theme/resto/');?>js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="<?php echo base_url('theme/resto/');?>js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="<?php echo base_url('theme/resto/');?>js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="<?php echo base_url('theme/resto/');?>js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="<?php echo base_url('theme/resto/');?>js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="<?php echo base_url('theme/resto/');?>js/owl.carousel.min.js"></script>
    <!-- swiper js -->
    <script src="<?php echo base_url('theme/resto/');?>js/slick.min.js"></script>
    <script src="<?php echo base_url('theme/resto/');?>js/gijgo.min.js"></script>
    <script src="<?php echo base_url('theme/resto/');?>js/jquery.nice-select.min.js"></script>
    <!-- custom js -->
    <script src="<?php echo base_url('theme/resto/');?>js/custom.js"></script>