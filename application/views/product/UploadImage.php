<div class="col-md-6">
	<?php echo form_open_multipart('ImageUpload/do_upload');?> 
	<form method="post" action="<?=base_url('ImageUpload/save_img')?>" class="panel form-horizontal form-bordered" enctype="multipart/form-data">
		<div class="panel-heading">
			<span class="panel-title">Upload Image</span>
		</div>

		<?php if($this->session->flashdata('upload_ok')){?>
			<div class="alert alert-success dark">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Well done!</strong> <?=$this->session->flashdata('upload_ok')?>
			</div>
		<?php } ?>

		<?php if($this->session->flashdata('upload_failed')){?>
			<div class="alert alert-warning dark">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Oops!</strong> <?=$this->session->flashdata('upload_failed')?>
			</div>
		<?php } ?>

		<div class="note note-success">
			<?php
				foreach ($product_details as $p_info) {
					$pid =  $p_info->product_id;
					$p_name = $p_info->description;
					echo '<p>Upload Image for: <strong>' . $p_info->description . '</strong></p>';
				}
			?>
		</div>

		<div class="panel-body no-padding-hr">
			<div class="form-group no-margin-hr no-margin-b panel-padding-h">
				<div class="row">
					<input type="hidden" name="product_id" value="<?=$pid?>">
					<input type="hidden" name="product_name" value="<?=$p_name?>">
					<label class="col-sm-4 control-label">Image File:</label>
					<div class="col-sm-8">
						<input type="file" name="userfile" class="form-control" required>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer text-right">
			<input class="btn btn-success" type = "submit" value = "Upload" /> 
			<a href="<?php echo base_url('products');?>" class="btn btn-warning">Back</a>
		</div>
	</form>
</div>
