				<script>
					init.push(function () {
						$('#product-category-table').dataTable();
						$('#product-category-table_wrapper .table-caption').text('Category List');
						$('#product-category-table_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
					});
				</script>

				<?php if($this->session->flashdata('del_err')){?>
					<div class="alert alert-danger dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Opps!</strong> <?php echo $this->session->flashdata('del_err'); ?>
					</div>
				<?php } ?>

				<?php if($this->session->flashdata('del_cat')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('del_cat'); ?>
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Products &rarr; Category</span>
						<a href="<?php echo base_url('products/addCategory');?>" class="btn btn-sm btn-primary btn-rounded pull-right">Add Category</a>
					</div>
					<div class="panel-body">
						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="product-category-table">
								<thead>
									<tr>
										<th>CATEGORY ID</th>
										<th>DESCRIPTION</th>
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($category_list as $rows) { ?>
									<tr class="odd gradeX">
										<td><?=$rows->cat_id?></td>
										<td><?=$rows->cat_name?></td>
										<td>
											<a class="btn btn-success btn-sm btn-rounded" href="<?php echo base_url('products/editCategory/'.$rows->cat_id);?>">Edit</a>

											<a class="btn btn-sm btn-rounded btn-danger" data-toggle="modal" data-target="#deleteCategory<?=$rows->cat_id?>">Delete</a>


											<div id="deleteCategory<?=$rows->cat_id?>" class="modal modal-alert modal-danger fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<i class="fa fa-times-circle"></i>
														</div>
														<div class="modal-title">You are about to delete </br> <?=$rows->cat_name?></div>
														<!-- <div class="modal-body">Click outside of this box to cancel.</div> -->
														<div class="modal-footer">
															<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
															<a class="btn btn-danger" href="<?php echo base_url('products/deleteCategory/'.$rows->cat_id);?>">Delete</a>
														</div>
													</div> 
												</div> 
											</div> 

											
										</td>
									</tr>		
									<?php }?>	
								</tbody>
							</table>
						</div>
					</div>
				</div>