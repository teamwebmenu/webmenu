				<script>
					init.push(function () {
						$('#product-table').dataTable();
						$('#product-table_wrapper .table-caption').text('Product List');
						$('#product-table_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

						$('#delete_product').on('show.bs.modal', function(e) {
					      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
					    });

					});

				</script>

				<?php if($this->session->flashdata('delete_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('delete_ok'); ?>
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Product Management</span>
						<a href="<?php echo base_url('products/addProduct');?>" class="btn btn-sm btn-primary btn-rounded pull-right">Add Product</a>
					</div>
					<div class="panel-body">
						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="product-table">
								<thead>
									<tr>
										<th>PRODUCT ID</th>
										<th>IMAGE</th>
										<th>NAME</th>
										<th>DESCRIPTION</th>
										<th>CATEGORY</th>
										<th>UNIT PRICE</th>
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($product_list as $rows){?>
									<tr class="odd gradeX">
										<td><?=$rows->product_id?></td>
										<td>
											<?php if($rows->upload_image){?>
												<img src="<?php echo base_url() . 'food_items/' . $rows->upload_image;?>" width="50;" height="50;">
											<?php }?>
										</td>
										<td><?=$rows->description?></td>
										<td><?=$rows->prod_desc?></td>
										<td><?=$rows->cat_name?></td>
										<td><?=number_format($rows->unit_price,2)?></td>
										<td>
											<a class="btn btn-success btn-sm btn-rounded" href="<?php echo base_url('products/editProduct/'.$rows->product_id);?>">Edit</a>

											<a class="btn btn-info btn-sm btn-rounded" href="<?php echo base_url('products/addProductPicture/'.$rows->product_id);?>">Upload Image</a>

											<a class="btn btn-sm btn-rounded btn-danger" data-toggle="modal" data-target="#delete_product<?=$rows->product_id?>">Delete</a>


											<div id="delete_product<?=$rows->product_id?>" class="modal modal-alert modal-danger fade">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<i class="fa fa-times-circle"></i>
														</div>
														<div class="modal-title">You are about to delete </br> <?=$rows->description?></div>
														<!-- <div class="modal-body">Click outside of this box to cancel.</div> -->
														<div class="modal-footer">
															<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
															<a class="btn btn-danger" href="<?php echo base_url('products/deleteProduct/'.$rows->product_id);?>">Delete</a>
														</div>
													</div> 
												</div> 
											</div> 

											
										</td>
									</tr>
									<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>


