			<div class="col-md-6">
				<script>
					init.push(function () {
						
						// Setup validation
						$("#frm_AddCategory").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								
								'cat_name': {
									required: true,
								},
							}
						});
					});
				</script>

				<?php if($this->session->flashdata('save_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> You successfully added new category.
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Product &rarr; Category</span>
					</div>
					<div class="panel-body">
						<div class="note note-info">Please Input New Product Category.</div>
						
						<form class="form-horizontal" id="frm_AddCategory" method="post" action="<?php echo base_url('products/saveCategory')?>">
							
							<div class="form-group dark">
								<label for="cat_name" class="col-sm-3 control-label">Category Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="cat_name" name="cat_name" placeholder="Category Name">
								</div>
							</div>


							<hr class="panel-wide">

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary btn-rounded">Save New Category</button>
									<a href="<?php echo base_url('products/category');?>" class="btn btn-warning btn-rounded">Cancel</a>
								</div>

							</div>
						</form>
					</div>
				</div>
		</div>