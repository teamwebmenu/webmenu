			<div class="col-md-6">
				<script>
					init.push(function () {
						
						// Setup validation
						$("#frm_AddCategory").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								
								'cat_name': {
									required: true,
								},
							}
						});
					});
				</script>

				<?php if($this->session->flashdata('cat_update')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('cat_update'); ?>
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Category &rarr; Edit</span>
					</div>
					<div class="panel-body">
						<div class="note note-info">Please type category name.</div>
						
						<form class="form-horizontal" id="frm_AddCategory" method="post" action="<?php echo base_url('products/updateCategory')?>">
							<?php foreach ($cat_details as $cat) {?>
							<input type="hidden" name="cat_id" value="<?=$cat->cat_id?>">
							<div class="form-group dark">
								<label for="cat_name" class="col-sm-3 control-label">Category Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="cat_name" name="cat_name" placeholder="Category Name" value="<?=$cat->cat_name?>">
								</div>
							</div>
							<?php }?>
							<hr class="panel-wide">

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary btn-rounded">Update</button>
									<a href="<?php echo base_url('products/category');?>" class="btn btn-warning btn-rounded">Cancel</a>
								</div>

							</div>
						</form>
					</div>
				</div>
		</div>