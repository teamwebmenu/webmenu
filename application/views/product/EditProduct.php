			<div class="col-md-6">
				<script>
					init.push(function () {
						
						$("#frm_addProduct").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'cat_id': {
									required: true,
								},
								'description': {
									required: true,
								},
								'prod_desc': {
									required: true,
								},
								'unit_price': {
									required: true,
									number: true
								},
							}
						});
					});
				</script>

				<?php if($this->session->flashdata('update_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> <?php echo $this->session->flashdata('update_ok'); ?>
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Product &rarr; Edit</span>
					</div>
					<div class="panel-body">
						<div class="note note-info">Please type new product details.</div>
						
						<form class="form-horizontal" id="frm_addProduct" method="post" action="<?php echo base_url('products/updateProduct')?>">
							<?php foreach($prod_details as $row){?>
								<input type="hidden" name="prod_id" value="<?=$row->product_id?>">
							<div class="form-group dark">
								<label for="description" class="col-sm-3 control-label">Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="description" name="description" value="<?=$row->description?>">
								</div>
							</div>

							<div class="form-group dark">
								<label for="doc_type" class="col-sm-3 control-label">Category</label>
								<div class="col-sm-9">
									<select class="form-control" name="cat_id" id="cat_id">
										<option></option>
										<?php foreach ($category_list as $cat) {?>
											<option 
												<?php if($row->cat_id==$cat->cat_id){ echo 'selected'; }?>
												value="<?=$cat->cat_id?>"><?=$cat->cat_name?>
											</option>	
										<?php }?>							
									</select>
								</div>
							</div>

							<div class="form-group dark">
								<label for="prod_desc" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="prod_desc" name="prod_desc" value="<?=$row->prod_desc?>">
								</div>
							</div>

							<div class="form-group dark">
								<label for="unit_price" class="col-sm-3 control-label">Unit Price</label>
								<div class="col-sm-9">
									<input type="number" class="form-control" id="unit_price" name="unit_price" value="<?=$row->unit_price?>">
								</div>
							</div>
						<?php }?>
							<hr class="panel-wide">

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary btn-rounded">Save New Product</button>
									<a href="<?php echo base_url('products');?>" class="btn btn-warning btn-rounded">Cancel</a>
								</div>

							</div>
						</form>
					</div>
				</div>
		</div>