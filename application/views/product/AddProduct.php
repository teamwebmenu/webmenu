			<div class="col-md-6">
				<script>
					init.push(function () {
						
						$("#frm_addProduct").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'cat_id': {
									required: true,
								},
								'description': {
									required: true,
								},
								'prod_desc': {
									required: true,
								},
								'unit_price': {
									required: true,
									number: true
								},
							}
						});
					});
				</script>

				<?php if($this->session->flashdata('save_ok')){?>
					<div class="alert alert-success dark">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>Well done!</strong> You successfully added new product.
					</div>
				<?php } ?>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Product &rarr; Product</span>
					</div>
					<div class="panel-body">
						<div class="note note-info">Please Input New Product.</div>
						
						<form class="form-horizontal" id="frm_addProduct" method="post" action="<?php echo base_url('products/saveProduct')?>">
							
							<div class="form-group dark">
								<label for="description" class="col-sm-3 control-label">Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="description" name="description">
								</div>
							</div>

							<div class="form-group dark">
								<label for="doc_type" class="col-sm-3 control-label">Category</label>
								<div class="col-sm-9">
									<select class="form-control" name="cat_id" id="cat_id">
										<option></option>
										<?php foreach ($cat_list as $cat) {?>
											<option value="<?=$cat->cat_id?>"><?=$cat->cat_name?></option>	
										<?php }?>							
									</select>
								</div>
							</div>

							<div class="form-group dark">
								<label for="prod_desc" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="prod_desc" name="prod_desc" maxlength="50">
								</div>
							</div>

							<div class="form-group dark">
								<label for="unit_price" class="col-sm-3 control-label">Unit Price</label>
								<div class="col-sm-9">
									<input type="number" class="form-control" id="unit_price" name="unit_price">
								</div>
							</div>

							<hr class="panel-wide">

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary btn-rounded">Save New Product</button>
									<a href="<?php echo base_url('products');?>" class="btn btn-warning btn-rounded">Cancel</a>
								</div>

							</div>
						</form>
					</div>
				</div>
		</div>