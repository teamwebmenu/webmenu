				<script>
					init.push(function () {
						$('#orders-table').dataTable();
						$('#orders-table_wrapper .table-caption').text('Order List');
						$('#orders-table_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
					});

					

				</script>

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Order Management</span>
					</div>
					<div class="panel-body">

						<?php if($this->session->flashdata('ack_ok')){?>
							<div class="alert alert-success dark" id="divNotiv">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<strong>Well done!</strong> <?php echo $this->session->flashdata('ack_ok'); ?>
							</div>
						<?php } ?>

						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="orders-table">
								<thead>
									<tr>
										<th>ORDER ID</th>
										<th>DATE CREATED</th>
										<th>CUSTOMER NAME</th>
										<th>NUMBER OF ITEMS</th>
										<th>TOTAL AMOUNT</th>
										<th>NOTES</th>
										<th>ORDER STATUS</th>
										<!-- <th>PAYMENT STATUS</th> -->
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($order_list as $list) { ?>
									<tr class="odd gradeX">
										<td><?=$list->order_id?></td>
										<td><?=date("F d, Y g:i A",strtotime($list->date_created))?></td>
										<td><?=$list->customer_name?></td>
										<td><?=$list->item_count?></td>
										<td><?=number_format($list->total_amount,2)?></td>
										<td><?=$list->notes?></td>
										<td>
											<?php
												if($list->order_status==0){
													echo '<span class="label label-warning label-tag">Pending</span>';
												}elseif($list->order_status==1){
													echo '<span class="label label-info label-tag">Submitted</span>';
												}elseif($list->order_status==2){
													echo '<span class="label label-success label-tag">Acknowledged</span>';
												}elseif($list->order_status==3){
													echo '<span class="label label-danger label-tag">Cancelled</span>';
												}elseif($list->order_status==4){
													echo '<span class="label label-primary label-tag">For Pickup</span>';
												}elseif($list->order_status==5){
													echo '<span class="label label-success label-tag">Order Paid</span>';
												}
											?>		
										</td>
										<!-- <td><?=($list->is_paid==0)?'Unpaid':'Paid';?></td> -->
										<td class="center">
											<a href="<?php echo base_url('order/myOrderDetails/'. $list->order_id);?>"class="btn btn-xs btn-rounded btn-info">View Items</a>
											
											<?php  if($list->order_status==1){//IF ORDER IS ACKNOWLEDGED ?>
												<a href="<?php echo base_url('order/acknowledgeOrder/'. $list->order_id);?>" class="btn btn-xs btn-rounded btn-success">Acknowledge Order</a>
											<?php } ?>

											<?php  if($list->order_status==2){//IF ORDER IS READY ?>
												<a href="<?php echo base_url('order/readyOrder/'. $list->order_id);?>" class="btn btn-xs btn-rounded btn-success">Ready for Pickup</a>
											<?php } ?>

											<?php  if($list->order_status==4){//IF ORDER IS FOR PAYMENT ?>
												<a href="<?php echo base_url('order/payOrder/'. $list->order_id);?>" class="btn btn-xs btn-rounded btn-success">Pay Order</a>
											<?php } ?>

											<?php  if($list->order_status==5){//IF ORDER IS PAD ?>
												<a href="<?php echo base_url('order/viewReceipt/'. $list->order_id);?>" target="_blank" class="btn btn-xs btn-rounded btn-success">View Receipt</a>
											<?php } ?>
											
											<!-- <a href="#" class="btn btn-xs btn-rounded btn-warning">Cancel</a> -->
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>