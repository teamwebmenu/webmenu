		<script>
			init.push(function () {
				$('#table_prod').dataTable();
				//$('#table_prod_wrapper .table-caption').text('Order List');
				$('#table_prod_wrapper .dataTables_filter input').attr('placeholder', 'Search product');
			});
		</script>

		<div class="com-md-12">
			<div class="col-md-6">
				<h1>Customer Order #999999</h1>
				<button class="btn btn-success btn-rounded">New Order</button>
				<div class="panel widget-notifications">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-fire"></i>Product List</span>
					</div>
					<div class="panel-body padding-sm">
						<div class="notifications-list">
							<div class="table-primary">
								<table class="table table-striped table-bordered" id="table_prod">
									<thead>
										<tr>
											<th class="text-center">Product</th>
											<th class="text-center">Category</th>
											<th class="text-center">Price</th>
											<th class="text-center">Add to Cart</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($prod_list as $prod) { ?>
										<tr>
											<td><?=$prod->description?></td>
											<td class="text-center"><?=$prod->cat_name?></td>
											<td class="text-center"><?=number_format($prod->unit_price,2)?></td>
											<td class="text-center">
												<button class="btn btn-sm btn-success" href="#"><span class="fa fa-plus"></span></button>
											</td>
										</tr>
									<?php }?>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
				<h1>Order Cart</h1>
				<div class="panel widget-notifications">
					<div class="panel-heading">
						<span class="panel-title"><i class="panel-title-icon fa fa-fire"></i>Order Summary</span>
					</div>
					<div class="panel-body padding-sm">
						<div class="notifications-list">
							<div class="table-primary">
								<table class="table table-striped table-bordered" id="table_prod">
									<thead>
										<tr>
											<th class="text-center">Product</th>
											<th class="text-center">Price</th>
											<th class="text-center">Quantity</th>
											<th class="text-center">Unit Cost</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>COKE IN CAN 330ML</td>
											<td class="text-center">35.00</td>
											<td class="text-center">2</td>
											<td class="text-center">70.00</td>
											<td class="text-center">
												<button class="btn btn-sm btn-danger" href="#"><span class="fa fa-minus"></span></button>
												<button class="btn btn-sm btn-info" href="#"><span class="fa fa-edit"></span></button>
											</td>
										</tr>
										<tr>
											<td>COKE IN CAN 330ML</td>
											<td class="text-center">35.00</td>
											<td class="text-center">2</td>
											<td class="text-center">70.00</td>
											<td class="text-center">
												<button class="btn btn-sm btn-danger" href="#"><span class="fa fa-minus"></span></button>
												<button class="btn btn-sm btn-info" href="#"><span class="fa fa-edit"></span></button>
											</td>
										</tr>
										<tr>
											<td>COKE IN CAN 330ML</td>
											<td class="text-center">35.00</td>
											<td class="text-center">2</td>
											<td class="text-center">70.00</td>
											<td class="text-center">
												<button class="btn btn-sm btn-danger" href="#"><span class="fa fa-minus"></span></button>
												<button class="btn btn-sm btn-info" href="#"><span class="fa fa-edit"></span></button>
											</td>
										</tr>
										<tr>
											<td>COKE IN CAN 330ML</td>
											<td class="text-center">35.00</td>
											<td class="text-center">2</td>
											<td class="text-center">70.00</td>
											<td class="text-center">
												<button class="btn btn-sm btn-danger" href="#"><span class="fa fa-minus"></span></button>
												<button class="btn btn-sm btn-info" href="#"><span class="fa fa-edit"></span></button>
											</td>
										</tr>
										<tr>
											<td colspan="3" class="text-right"><strong>TOTAL AMOUNT</strong></td>
											<td class="text-center"><strong>140.00</strong></td>
											<td class="text-center"><button class="btn btn-success">Checkout</button></td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
