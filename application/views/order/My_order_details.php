				<script>
					init.push(function () {
						$('#orders-table').dataTable();
						$('#orders-table_wrapper .table-caption').text('Order Number: <?=$order_id?>');
						$('#orders-table_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
					});
				</script>

				<div class="panel">
					<div class="panel-heading">
						<h3>Order Details</h3>
						<span class="panel-title">
							<?php if($this->session->userdata('access_level')==2) {?>
							<a class="btn btn-rounded btn-info" href="<?php echo base_url('order/myOrder/'.$this->session->userdata('user_id'));?>">Back</a>
							<a class="btn btn-rounded btn-success" href="<?php echo base_url('order/addItems_this_Cart/'.$order_id);?>">Add Items</a>
						<?php }else{?>
							<a class="btn btn-rounded btn-info" href="<?php echo base_url('order');?>">Back</a>
						<?php } ?>
						</span>
					</div>
					<div class="panel-body">

						<?php if($this->session->flashdata('remove_ok')){?>
							<div class="alert alert-success dark">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<strong>Well done!</strong> <?php echo $this->session->flashdata('remove_ok'); ?>
							</div>
						<?php } ?>

						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="orders-table">
								<thead>
									<tr>
										<th>PRODUCT DESCRIPTION</th>
										<th>QUANTITY</th>
										<th>PRICE</th>
										<?php if($this->session->userdata('access_level')==2) {?>
										<th>ACTION</th>
										<?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($my_orders as $list) { ?>
									<tr class="odd gradeX">
										<td><?=$list->description?></td>
										<td><?=$list->quantity?></td>
										<td><?=number_format($list->unit_price,2)?></td>

										<?php if($this->session->userdata('access_level')==2) {?>
										<td class="center">
											<a href="<?php echo base_url('order/removeItem/'. $list->id);?>"class="btn btn-xs btn-rounded btn-danger">Remove</a>	
										</td>
										<?php } ?>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>