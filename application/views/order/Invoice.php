<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Order Receipt - Lola Tanang's Restaurant</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Open Sans font from Google CDN -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

	<!-- Pixel Admin's stylesheets -->
	<link href="<?php echo base_url();?>theme/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>theme/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>theme/assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>theme/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->

</head>


<body class="page-invoice page-invoice-print">
	<script>
		window.onload = function () {
			window.print();
		};
	</script>

	<div class="invoice">
		<div class="invoice-header">
			<h3>
				<div>
					<small><strong>Lola Tanang's </strong>Restaurant</small><br>
					ORDER RECEIPT
				</div>
			</h3>
			<address>
				Lola Tanang's Restaurant<br>
				Vicentillo Street, Municipality of Naval<br>
				Province of Biliran
			</address>
			<?php foreach ($invoice_header as $head) { ?>
			<div class="invoice-date">
				<small><strong>Date</strong></small><br>
				<?=date("F d, Y g:i A",strtotime($head->date_paid))?>
			</div>
		</div> <!-- / .invoice-header -->
		<div class="invoice-info">
			<div class="invoice-recipient">
				<strong><?=$head->customer_name?></strong><br>
				<?=$head->customer_street?>, <?=$head->customer_barangay?><br>
				<?=$head->customer_municipality?>, Biliran Province<br>
				<?=$head->contact_number?>
			</div> <!-- / .invoice-recipient -->
			<div class="invoice-total">
				<span>Php <?=number_format($head->total_cost,2)?></span>
				TOTAL:
			</div>
			<?php  }?> <!-- / .invoice-total -->
		</div> <!-- / .invoice-info -->
		<hr>
		<div class="invoice-table">
			<table>
				<thead>
					<tr>
						<th>ITEM DESCRIPTION</th>
						<th>QUANTITY</th>
						<th>UNIT PRICE</th>
						<th>TOTAL PRICE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($invoice_details as $details) { ?>
					<tr>
						<td><?=$details->description?></td>
						<td><?=$details->quantity?></td>
						<td><?=number_format($details->unit_price,2)?></td>
						<td>
							<?=number_format($details->unit_price * $details->quantity,2)?>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div> <!-- / .invoice-table -->
	</div> <!-- / .invoice -->

</body>
</html>