				<script>
					init.push(function () {
						$('#orders-table').dataTable();
						$('#orders-table_wrapper .table-caption').text('My Order Cart');
						$('#orders-table_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

					});

				</script>

				<div class="panel">
					<div class="panel-heading">
						<h3>My Order Cart</h3>
						<a class="btn btn-rounded btn-success" href="<?php echo base_url('order/newOrder');?>">Create New Order</a>
					</div>
					<div class="panel-body">

						<?php if($this->session->flashdata('delete_ok')){?>
							<div class="alert alert-success dark" id="notif1">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<strong>Well done!</strong> <?php echo $this->session->flashdata('delete_ok'); ?>
							</div>
						<?php } ?>

						<?php if($this->session->flashdata('submit_ok')){?>
							<div class="alert alert-success dark">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<strong>Well done!</strong> <?php echo $this->session->flashdata('submit_ok'); ?>
							</div>
						<?php } ?>

						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="orders-table">
								<thead>
									<tr>
										<th>ORDER ID</th>
										<th>DATE CREATED</th>
										<th>CUSTOMER NAME</th>
										<th>NUMBER OF ITEMS</th>
										<th>TOTAL AMOUNT</th>
										<th>NOTES</th>
										<th>ORDER STATUS</th>
										<!-- <th>PAYMENT STATUS</th> -->
										<th>ACTION</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($my_orders as $list) { ?>
									<tr class="odd gradeX">
										<td><?=$list->cart_id?></td>
										<td><?=date("F d, Y g:i A",strtotime($list->date_created))?></td>
										<td><?=$list->customer_name?></td>
										<td><?=$list->item_count?></td>
										<td><?=number_format($list->total_amount,2)?></td>
										<td><?=$list->notes?></td>
										<td>
											<?php
												if($list->order_status==0){
													echo '<span class="label label-warning label-tag">Pending</span>';
												}elseif($list->order_status==1){
													echo '<span class="label label-info label-tag">Submitted</span>';
												}elseif($list->order_status==2){
													echo '<span class="label label-success label-tag">Acknowledged</span>';
												}elseif($list->order_status==3){
													echo '<span class="label label-danger label-tag">Cancelled</span>';
												}elseif($list->order_status==4){
													echo '<span class="label label-primary label-tag">For Pickup</span>';
												}elseif($list->order_status==5){
													echo '<span class="label label-success label-tag">Order Paid</span>';
												}
											?>			
										</td>
										<!-- <td><?=($list->is_paid==0)?'Unpaid':'Paid';?></td> -->
										<td class="center">

											<?php if($list->order_status==0){ ?>
											<a href="<?php echo base_url('order/myOrderDetails/'. $list->cart_id);?>"class="btn btn-xs btn-rounded btn-info">View Items</a>
											<!-- <a href="<?php echo base_url('order/submitOrder/'. $list->cart_id);?>" class="btn btn-xs btn-rounded btn-success" >Submit Order</a> -->

											<button class="btn btn-xs btn-rounded btn-success" data-toggle="modal" data-target="#submit<?=$list->cart_id?>">Submit Order</button>

											<a class="btn btn-xs btn-danger btn-rounded" data-toggle="modal" data-target="#cancel_order<?=$list->cart_id?>">Cancel Order</a>


											<div id="submit<?=$list->cart_id?>" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
												<div class="modal-dialog modal-md">
													<div class="modal-content">
														<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														<h4 class="modal-title">Confirm Order</h4>
														</div>
														<div class="modal-body">
															<div class="col-md-12">
															<h4>Order #: <strong><?=$list->cart_id?></strong></h4>
															<?php 
																$cart_objects = $this->Model_orders->order_details_on_submit($list->cart_id);
															?>
																<table class="table">
																	<thead>
																		<th>Item/Description</th>
																		<th>Quantity</th>
																		<th>Unit Price</th>
																		<th>Total Price</th>
																	</thead>
																	<tbody>
																		<?php 
																			$amt = 0;
																			foreach ($cart_objects as $value) { 
																			$amt += $value->total_price;
																		?>
																		<tr>
																			<td><?=$value->description?></td>
																			<td><?=$value->quantity?></td>
																			<td><?=number_format($value->unit_price,2)?></td>
																			<td><?=number_format($value->total_price,2)?></td>
																		</tr>
																		<?php } ?>
																		<tr>
																			<td colspan="3"><strong>Total Amount Due</strong></td>
																			<td><strong><?=number_format($amt,2)?></strong></td>
																		</tr>
																	</tbody>
																</table>
															</div>
															<div class="col-md-12">
															<hr class="panel-wide">
															<form class="form-horizontal" id="frm_addUser" method="post" action="<?php echo base_url('order/submitOrder');?>">

																<div class="form-group dark">
																	<input type="hidden" name="cart_id" value="<?=$list->cart_id?>">
																	<label for="position" class="control-label">Notes:</label>
																	<div class="col-sm-12">
																		<textarea class="form-control" id="order_notes" name="order_notes" placeholder="Order Notes" cols="70"></textarea>
																	</div>
																</div>	

																<hr class="panel-wide">

																<div class="form-group" style="padding-bottom: 20px;">
																	<div class="col-sm-12">
																		<button type="submit" class="btn btn-success btn-rounded">Confirm Order and Submit</button>
																	</div>
																</div>
															</form>

															</div>
														</div>
													</div> 
												</div> 
											</div>


											<?php } ?>

											<?php if($list->order_status==5){ ?>
												<a href="<?php echo base_url('order/viewReceipt/'. $list->cart_id);?>"class="btn btn-xs btn-rounded btn-success" target="_blank">View Receipt</a>
											<?php } ?>
											
											<div id="cancel_order<?=$list->cart_id?>" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
												<div class="modal-dialog modal-sm">
													<div class="modal-content">
														<div class="modal-header">
														<h4 class="modal-title">Confirm Order Cancel</h4>
														</div>
														<div class="modal-body">
															<div>
																<p>Are you sure you want to cancel this order <strong><?=$list->cart_id?></strong>?</p>
															</div>
															<div class="row">
																<a href="<?php echo base_url('order/delete_order/'.$list->cart_id);?>" class="btn btn-xs btn-danger btn-rounded">Yes, cancel order</a>
																<button class="btn btn-xs btn-success btn-rounded pull-right" data-dismiss="modal">No</button>
															</div>
														</div>
													</div>
												</div> 
											</div> 
											

										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>