
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Sign Up - Lola Tanang's</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Open Sans font from Google CDN -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

	<!-- Pixel Admin's stylesheets -->
	<link href="<?php echo base_url();?>theme/assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>theme/assets/stylesheets/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>theme/assets/stylesheets/pages.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>theme/assets/stylesheets/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>theme/assets/stylesheets/themes.min.css" rel="stylesheet" type="text/css">

	<!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->



</head>

<!-- Pixel Admin's javascripts -->
<script src="<?php echo base_url();?>theme/assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/javascripts/pixel-admin.min.js"></script>

<script type="text/javascript">
	// function keyCheck(){
	// 	var gen_captcha = $("#gen_captcha").val();
	// 	var inp_captcha = $("#captcha").val();

	// 	if(gen_captcha!=inp_captcha){
	// 		alert('Invalid security key!');
	// 		location.reload();

	// 	}
	// }

	function changeMunicipal(){
		
  		var  val = $("#municipality").val();
  		$("#barangay").val("");

  		if(val=="Biliran"){
  			$("#barangay").html("<option value=''></option><option value='Bato'>Bato</option><option value='Burabod'>Burabod</option><option value='Busali'>Busali</option><option value='Hugpa'>Hugpa</option><option value='Julita'>Julita</option><option value='Canila'>Canila</option><option value='Pinangumhan'>Pinangumhan</option><option value='San Isidro'>San Isidro</option><option value='Sanggalang'>Sanggalang</option><option value='Villa Enage'>Villa Enage</option>");
  		}

  		if(val=="Naval"){
  			$("#barangay").html("<option value=''></option><option value='Agpangi'>Agpangi</option><option value='Anislagan'>Anislagan</option><option value='Atipolo'>Atipolo</option><option value='Calumpang'>Calumpang</option><option value='Capinahan'>Capinahan</option><option value='Caraycaray'>Caraycaray</option><option value='Catmon'>Catmon</option><option value='Haguikhikan'>Haguikhikan</option><option value='Padre Inocentes Garcia'>Padre Inocentes Garcia</option><option value='Libertad'>Libertad</option><option value='Lico'>Lico</option><option value='Lucsoon'>Lucsoon</option><option value='Mabini'>Mabini</option><option value='San Pablo	'>San Pablo	</option><option value='Santo Nino'>Santo Nino</option><option value='Smo Rosario'>Smo Rosario</option><option value='Talustusan'>Talustusan</option><option value='Villa Caneja'>Villa Caneja</option><option value='Villa Consuelo'>Villa Consuelo</option><option value='Borac'>Borac</option><option value='Cabungaan'>Cabungaan</option><option value='Imelda'>Imelda</option><option value='Larrazabal'>Larrazabal</option><option value='Libtong'>Libtong</option><option value='Padre Sergio Eamiguel'>Padre Sergio Eamiguel</option><option value='Sabang'>Sabang</option>");
  		}

  		if(val=="Almeria"){
  			$("#barangay").html("<option value=''></option><option value='Caucab'>Caucab</option><option value='Iyosan'>Iyosan</option><option value='Jamorawon'>Jamorawon</option><option value='Look'>Look</option><option value='Matango'>Matango</option><option value='Pili'>Pili</option><option value='Poblacion'>Poblacion</option><option value='Pulang Bato'>Pulang Bato</option><option value='Salangi'>Salangi</option><option value='Sampao'>Sampao</option><option value='Tabunan'>Tabunan</option><option value='Talahid'>Talahid</option><option value='Tamarindo'>Tamarindo</option>");
  		}

  		if(val=="Cabucgayan"){
  			$("#barangay").html("<option value=''></option><option value='Balaquid'>Balaquid</option><option value='Baso'>Baso</option><option value='Bunga'>Bunga</option><option value='Caanibongan'>Caanibongan</option><option value='Casiawan'>Casiawan</option><option value='Esperanza'>Esperanza</option><option value='Langgao'>Langgao</option><option value='Libertad'>Libertad</option><option value='Looc'>Looc</option><option value='Magbangon'>Magbangon</option><option value='Pawikan'>Pawikan</option><option value='Salawad'>Salawad</option><option value='Talibong'>Talibong</option>");
  		}

  		if(val=="Caibiran"){
  			$("#barangay").html("<option value=''></option><option value='Alegria'>Alegria</option><option value='Asug'>Asug</option><option value='Bari-is'>Bari-is</option><option value='Binohangan'>Binohangan</option><option value='Cabibihan'>Cabibihan</option><option value='Kawayanon'>Kawayanon</option><option value='Looc'>Looc</option><option value='Manlabang'>Manlabang</option><option value='Caulangohan'>Caulangohan</option><option value='Maurang'>Maurang</option><option value='Palanay'>Palanay</option><option value='Palengke'>Palengke</option><option value='Tomalistis'>Tomalistis</option><option value='Union'>Union</option><option value='Uson'>Uson</option><option value='Victory'>Victory</option><option value='Villa Vicenta'>Villa Vicenta</option>");
  		}

  		if(val=="Culaba"){
  			$("#barangay").html("<option value=''></option><option value='Acaban'>Acaban</option><option value='Bacolod'>Bacolod</option><option value='Binongtoan'>Binongtoan</option><option value='Bool Central'>Bool Central</option><option value='Bool East'>Bool East</option><option value='Bool West'>Bool West</option><option value='Calipayan'>Calipayan</option><option value='Guindapunan'>Guindapunan</option><option value='Habuhab'>Habuhab</option><option value='Looc'>Looc</option><option value='Marvel'>Marvel</option><option value='Patag'>Patag</option><option value='Pinamihagan'>Pinamihagan</option><option value='Culaba Central'>Culaba Central</option><option value='Salvacion'>Salvacion</option><option value='San Roque'>San Roque</option><option value='Virginia'>Virginia</option>");
  		}

  		if(val=="Kawayan"){
  			$("#barangay").html("<option value=''></option><option value='Baganito'>Baganito</option><option value='Balacson'>Balacson</option><option value='Balite'>Balite</option><option value='Bilwang'>Bilwang</option><option value='Buyo'>Buyo</option><option value='Bulalacao'>Bulalacao</option><option value='Burabod'>Burabod</option><option value='Inasuyan'>Inasuyan</option><option value='Kansanok'>Kansanok</option><option value='Mada-o'>Mada-o</option><option value='Mapuyo'>Mapuyo</option><option value='Masagaosao'>Masagaosao</option><option value='Masagongsong'>Masagongsong</option><option value='Poblacion'>Poblacion</option><option value='Tabunan North'>Tabunan North</option><option value='Tubig Guinoo</option><option value='Tucdao'>Tucdao</option><option value='Ungale'>Ungale</option><option value='Villa Cornejo'>Villa Cornejo</option><option value='San Lorenzo'>San Lorenzo</option>");
  		}

  		if(val=="Maripipi"){
  			$("#barangay").html("<option value=''></option><option value='Agutay'>Agutay</option><option value='Banlas'>Banlas</option><option value='Bato'>Bato</option><option value='Binalayan West'>Binalayan West</option><option value='Binalayan East'>Binalayan East</option><option value='Burabod'>Burabod</option><option value='Calbani'>Calbani</option><option value='Canduhao'>Canduhao</option><option value='Casibang'>Casibang</option><option value='Danao'>Danao</option><option value='Ol-og'>Ol-og</option><option value='Binongto-an'>Binongto-an</option><option value='Ermita'>Ermita</option><option value='Trabugan'>Trabugan</option><option value='Viga'>Viga</option>");
  		}
	
	}

</script>

<body class="theme-default page-signup">

	<!-- Page background -->
	<div id="page-signup-bg">
		<!-- Background overlay -->
		<div class="overlay"></div>
		<!-- Replace this with your bg image -->
		<img src="<?php echo base_url();?>theme/assets/demo/signin-bg-1.jpg" alt="">
	</div>


	<!-- Container -->
	<div class="signup-container">
		<!-- Header -->
		<div class="signup-header">
			<a href="<?php echo base_url();?>" class="logo">
				<img src="<?php echo base_url();?>theme/assets/demo/logo-big.png" alt="" style="margin-top: -5px;">&nbsp;
				Lola Tanang's Restaurant
			</a> <!-- / .logo -->
			<div class="slogan">
				Home. Cooked. Delicacies.
			</div> <!-- / .slogan -->

			<?php if($this->session->flashdata('customer_ok')){?>
					<div class="alert alert-success dark">
						<strong>Well done!</strong> Registration sucessfull!. Your mobile number is your username.
					</div>
			<?php } ?>

			<?php if($this->session->flashdata('customer_exists')){?>
					<div class="alert alert-danger dark">
						<strong>Registration Failed!</strong> <?php echo $this->session->flashdata('customer_exists'); ?>
					</div>
			<?php } ?>

			<?php if($this->session->flashdata('invalid_captcha')){?>
					<div class="alert alert-warning dark">
						<strong>Opps!</strong> <?php echo $this->session->flashdata('invalid_captcha'); ?>
					</div>
			<?php } ?>

		</div>
		<!-- / Header -->

		<!-- Form -->
		<div class="signup-form">
			<form action="<?php echo base_url('register/new_customer');?>" id="frm_register" method="post">
				
				<div class="signup-text">
					<span>Create an account</span>
				</div>

				<div class="form-group w-icon">
					<input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" required>
					<span class="fa fa-info signup-form-icon"></span>
				</div>

				<div class="form-group w-icon">
					<input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" required>
					<span class="fa fa-info signup-form-icon"></span>
				</div>

				<div class="form-group w-icon">
					<label for="province">Province</label>
					<select class="form-control input-lg" name="province" id="province" placeholder="Province" required>
							<option value="Biliran Province" selected>Biliran</option>
					</select>
				</div>

				<div class="form-group w-icon">
					<label for="municipality">Municipality</label>
					<select class="form-control input-lg" name="municipality" id="municipality" placeholder="Municipality" onchange="changeMunicipal()"  required>
							<option value=""></option>
							<option value="Almeria">Almeria</option>
							<option value="Biliran">Biliran</option>
							<option value="Cabucgayan">Cabucgayan</option>
							<option value="Caibiran">Caibiran</option>
							<option value="Culaba">Culaba</option>
							<option value="Kawayan">Kawayan</option>
							<option value="Maripipi">Maripipi</option>
							<option value="Naval">Naval</option>
					</select>
				</div>

				<div class="form-group w-icon">
					<label for="province">Barangay</label>
					<select class="form-control input-lg" name="barangay" id="barangay" placeholder="Barangay" required>
							<option value=""></option>
					</select>
				</div>


				<div class="form-group w-icon">
					<label for="address">House No./Street/Subdivision</label>
					<textarea name="address" id="address" class="form-control input-lg" required></textarea>
					<!-- <span class="fa fa-home signup-form-icon"></span> -->
				</div>
				
			

				<div class="form-group w-icon">
					<input type="text" name="mobile_number" id="mobile_number" class="form-control input-lg" placeholder="Mobile Number" required>
					<span class="fa fa-phone signup-form-icon"></span>
				</div>

				<div class="form-group w-icon">
					<input type="text" name="email" id="email" class="form-control input-lg" placeholder="Email Address" required>
					<span class="fa fa-envelope signup-form-icon"></span>
				</div>


				<div class="form-group w-icon">
					<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" required>
					<span class="fa fa-lock signup-form-icon"></span>
				</div>

				<div class="form-group">
					<?php 
						$permitted_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  
						function generate_string($input, $strength = 5) {
						    $input_length = strlen($input);
						    $random_string = '';
						    for($i = 0; $i < $strength; $i++) {
						        $random_character = $input[mt_rand(0, $input_length - 1)];
						        $random_string .= $random_character;
						    }
						  
						    return $random_string;
						}
						 
						$string_length = 8 ;
						$captcha_string = generate_string($permitted_chars, $string_length);

					?>
					<input type="hidden" name="gen_captcha" id="gen_captcha" value="<?=$captcha_string?>">
					<label id="sec_key">Security Key: <strong><?=$captcha_string?></strong></label>
					<input type="text" name="captcha" id="captcha" class="form-control input-lg" placeholder="Input Security Key" required>
				</div>


				<div class="form-group" style="margin-top: 20px;margin-bottom: 20px;">
					<label class="checkbox-inline">
						<input type="checkbox" name="agree_tor" class="px" id="agree_tor" required>
						<span class="lbl">I agree with the <a href="<?php echo base_url('resto/tor');?>" target="_blank">Terms and Conditions</a></span>
					</label>
				</div>

				<div class="form-actions">
					<input type="submit" value="SIGN UP" class="signup-btn bg-primary" onclick="keyCheck()">
				</div>
			</form>
			<!-- / Form -->

		</div>
		<!-- Right side -->
	</div>

		<div class="have-account">
		Already have an account? <a href="<?php echo base_url('auth');?>">Sign In</a>
	</div>


<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<!-- <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">'+"<"+"/script>"); </script> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>theme/assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->




</body>
</html>
