
<!DOCTYPE html>
<!--[if IE 8]>         
  <html class="ie8"> 
<![endif]-->
<!--[if IE 9]>         
  <html class="ie9 gt-ie8"> 
<![endif]-->
<!--[if gt IE 9]><!--> 
<html class="gt-ie8 gt-ie9 not-ie"> 
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?=$page_title?></title>

  <?php $this->load->view('templates/styles'); ?>

</head>

<body class="theme-default main-menu-animated main-navbar-fixed">

<script>var init = [];</script>
<!-- <script src="assets/demo/demo.js"></script>
 -->
<div id="main-wrapper">

 <?php $this->load->view('templates/topbar'); ?>

 <?php $this->load->view('templates/sidebar'); ?>

  <div id="content-wrapper">

    <?php $this->load->view($content); ?>

  </div> 
  <div id="main-menu-bg"></div>
</div> 

  <?php $this->load->view('templates/script'); ?>

</body>
</html>