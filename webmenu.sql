/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : webmenu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-12-19 12:28:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('6', 'Soup');
INSERT INTO `categories` VALUES ('7', 'Chicken');
INSERT INTO `categories` VALUES ('8', 'Pork');
INSERT INTO `categories` VALUES ('9', 'Beef');
INSERT INTO `categories` VALUES ('10', 'Fish');
INSERT INTO `categories` VALUES ('11', 'Rice');
INSERT INTO `categories` VALUES ('12', 'Drinks');
INSERT INTO `categories` VALUES ('13', 'Pasta');
INSERT INTO `categories` VALUES ('14', 'Vegetable');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` varchar(50) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `order_type` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_paid` int(11) DEFAULT '0',
  `order_status` int(11) DEFAULT '0',
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('4311167014', '2019-12-19 10:53:37', null, '4', '0', '1', 'will pickup order at 4pm');
INSERT INTO `orders` VALUES ('41035754829', '2019-12-19 11:21:34', null, '4', '0', '1', 'submit');

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_details
-- ----------------------------
INSERT INTO `order_details` VALUES ('1', '4311167014', '1', '3', '60', '2019-12-19 10:53:37');
INSERT INTO `order_details` VALUES ('2', '4311167014', '2', '2', '300', '2019-12-19 10:53:39');
INSERT INTO `order_details` VALUES ('3', '4311167014', '3', '10', '10', '2019-12-19 10:53:42');
INSERT INTO `order_details` VALUES ('4', '41035754829', '2', '6', '300', '2019-12-19 11:21:34');
INSERT INTO `order_details` VALUES ('5', '41035754829', '4', '7', '15', '2019-12-19 11:21:36');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  `upload_image` varchar(255) DEFAULT 'default.jpg',
  `prod_desc` text,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Fish Fillet', '10', '60', '1.jpg', 'Juicy fresh tuna fish');
INSERT INTO `products` VALUES ('2', 'Fried Chicken (Whole)', '7', '300', '2.jpg', 'short desc');
INSERT INTO `products` VALUES ('3', 'Plain Rice', '11', '10', '3.jpg', 'short desc');
INSERT INTO `products` VALUES ('4', 'Java Rice', '11', '15', '4.jpg', 'short desc');
INSERT INTO `products` VALUES ('5', 'Beef Chowfan', '11', '49', '5.jpg', 'short desc');
INSERT INTO `products` VALUES ('6', 'Pork Chowfan', '11', '49', '6.jpg', 'short desc');
INSERT INTO `products` VALUES ('7', 'Yangchow', '11', '50', '7.jpg', 'short desc');
INSERT INTO `products` VALUES ('8', 'Pineapple Juice', '12', '35', '8.jpg', 'short desc');
INSERT INTO `products` VALUES ('9', 'Orange Juice', '12', '30', '9.jpg', 'short desc');
INSERT INTO `products` VALUES ('10', 'Coke (Litro)', '12', '25', '10.jpg', 'short desc');
INSERT INTO `products` VALUES ('11', 'Coke (Kasalo)', '12', '20', 'default.jpg', 'short desc');
INSERT INTO `products` VALUES ('12', 'Chicken Soup', '6', '50', 'default.jpg', 'short desc');
INSERT INTO `products` VALUES ('13', 'Chopsue', '14', '30', 'default.jpg', 'short desc');
INSERT INTO `products` VALUES ('15', 'Lechon Manok', '7', '240', '15.jpg', 'short desc');
INSERT INTO `products` VALUES ('18', 'asdfas', '11', '1222', 'default.jpg', 'the quick brow fox jumps over the lazy dogs');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `access_level` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) DEFAULT NULL,
  `province` text,
  `municipality` text,
  `barangay` text,
  `address` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'Tanang', 'Lola', null, 'Administrator', '1', '1', '2019-11-09 10:08:31', null, null, null, null, null, '1.jpg');
INSERT INTO `users` VALUES ('2', 'user', '5f4dcc3b5aa765d61d8327deb882cf99', 'Standard', 'User', null, 'User', '0', '1', '2019-11-09 10:08:31', null, null, null, null, null, null);
INSERT INTO `users` VALUES ('3', '09066787857', 'e10adc3949ba59abbe56e057f20f883e', 'rahnee', 'estrada', '09066787857', 'Customer', '2', '1', '2019-11-09 10:08:31', null, null, null, null, 'bulacan', null);
INSERT INTO `users` VALUES ('4', '0987654321', 'e10adc3949ba59abbe56e057f20f883e', 'James', 'Bond', '0987654321', 'Customer', '2', '1', '2019-11-19 13:57:16', null, 'Biliran Province', 'Kawayan', 'Poblacion', 'San Rafael Street', '4.jpg');
